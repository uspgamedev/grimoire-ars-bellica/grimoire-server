use super::definitions::*;

use grimoire_common::file::read_file;

use regex::Regex;

use std::io::Result;
use std::path::PathBuf;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Token {
    ValueIdentifier(String),
    MethodIdentifier(String),
    TypeIdentifier(Type),
    NumberLiteral(i64),
    LBracket,
    RBracket,
    LPar,
    RPar,
    Is,
    Separator,
    Colon,
    Comma,
    If,
    Then,
    Else,
    True,
    False,
}

pub fn tokenize_file(path: &PathBuf) -> Result<Vec<Vec<Token>>> {
    tokenize_string(read_file(path)?)
}

pub fn tokenize_string(string: String) -> Result<Vec<Vec<Token>>> {
    let token_lines = string
        .split("\n")
        .map(|line| tokenize_line(line.to_string()))
        .collect();
    Ok(token_lines)
}

// Internal functions

fn tokenize_line(line: String) -> Vec<Token> {
    if line == "~" {
        return vec![Token::Separator];
    }
    let mut tokens = Vec::default();
    for word in line.split_whitespace() {
        let mut current = word.clone();
        while !current.is_empty() {
            let (token, new_current) = scan_token(current);
            current = new_current;
            tokens.push(token);
        }
    }
    tokens
}

fn scan_token(current: &str) -> (Token, &str) {
    for (regex, expected_token) in REGEX_VEC.iter() {
        if let Some(mat) = regex.find(current) {
            let (matched_str, rest) = current.split_at(mat.end());
            let scanned_token = match_token(matched_str, expected_token);
            return (scanned_token, rest);
        }
    }
    panic!["Unknown token {:?}", current];
}

fn match_token(matched_str: &str, expected_token: &Token) -> Token {
    match expected_token {
        Token::ValueIdentifier(_) => match TYPE_TABLE.get(matched_str) {
            Some(ty) => Token::TypeIdentifier(ty.clone()),
            None => Token::ValueIdentifier(matched_str.to_string()),
        },
        Token::MethodIdentifier(_) => Token::MethodIdentifier(matched_str.to_string()),
        Token::NumberLiteral(_) => Token::NumberLiteral(matched_str.parse::<i64>().unwrap()),
        _ => expected_token.clone(),
    }
}

lazy_static! {
    static ref REGEX_VEC: Vec<(Regex, Token)> = vec![
        (Regex::new(r"^\[").unwrap(), Token::LBracket),
        (Regex::new(r"^\]").unwrap(), Token::RBracket),
        (Regex::new(r"^\(").unwrap(), Token::LPar),
        (Regex::new(r"^\)").unwrap(), Token::RPar),
        (Regex::new(r"^is").unwrap(), Token::Is),
        (Regex::new(r"^if").unwrap(), Token::If),
        (Regex::new(r"^then").unwrap(), Token::Then),
        (Regex::new(r"^else").unwrap(), Token::Else),
        (Regex::new(r"^true").unwrap(), Token::True),
        (Regex::new(r"^false").unwrap(), Token::False),
        (
            Regex::new(r"^\*").unwrap(),
            Token::MethodIdentifier(String::default())
        ),
        (
            Regex::new(r"^/").unwrap(),
            Token::MethodIdentifier(String::default())
        ),
        (
            Regex::new(r"^\+").unwrap(),
            Token::MethodIdentifier(String::default())
        ),
        (Regex::new(r"^:").unwrap(), Token::Colon),
        (Regex::new(r"^,").unwrap(), Token::Comma),
        (Regex::new(r"^\d+").unwrap(), Token::NumberLiteral(0)),
        (
            Regex::new(r"^[A-Z]\w*").unwrap(),
            Token::ValueIdentifier(String::default())
        ),
        (
            Regex::new(r"^[a-z]\w*\??").unwrap(),
            Token::MethodIdentifier(String::default())
        ),
    ];
}
