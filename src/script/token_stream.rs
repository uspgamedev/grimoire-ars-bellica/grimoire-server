use super::definitions::{MethodIdentifier, Type, ValueIdentifier};
use super::parser::invalid_input;
use super::tokenizer::Token;
use std::io::{Error, ErrorKind, Result};

#[derive(Debug)]
pub struct TokenStream {
    tokens: Vec<Token>,
    index: usize,
    stack: Vec<usize>,
}

impl TokenStream {
    pub fn new(tokens: Vec<Token>) -> Self {
        Self {
            tokens,
            index: 0,
            stack: Vec::default(),
        }
    }

    pub fn step(&mut self) {
        self.index += 1;
    }

    pub fn end(&self) -> bool {
        self.index >= self.tokens.len()
    }

    pub fn push(&mut self) {
        self.stack.push(self.index);
    }

    pub fn pop_and_rollback(&mut self) {
        self.index = self.stack.pop().unwrap();
    }

    pub fn pop(&mut self) {
        self.stack.pop();
    }

    pub fn get(&self) -> Option<&Token> {
        self.peek(0)
    }

    pub fn peek(&self, n: usize) -> Option<&Token> {
        if self.index + n >= self.tokens.len() {
            None
        } else {
            Some(&self.tokens[self.index + n])
        }
    }

    pub fn peek_check(&self, n: usize, check_token: Token) -> bool {
        if let Some(token) = self.peek(n) {
            *token == check_token
        } else {
            false
        }
    }

    pub fn get_identifier(&self) -> Option<ValueIdentifier> {
        match self.get() {
            Some(token) => match token {
                Token::ValueIdentifier(id) => Some(id.clone()),
                _ => None,
            },
            None => None,
        }
    }

    pub fn next(&mut self) -> Option<Token> {
        let token = self.get().cloned();
        self.step();
        token
    }

    pub fn accept(&mut self, token: &Token) -> bool {
        if !self.end() && self.get().unwrap() == token {
            self.step();
            true
        } else {
            false
        }
    }

    pub fn accept_value_identifier(&mut self) -> Option<ValueIdentifier> {
        if !self.end() {
            if let Token::ValueIdentifier(id) = self.get().unwrap() {
                let ident = id.clone();
                self.step();
                return Some(ident);
            }
        }
        None
    }

    pub fn accept_method_identifier(&mut self) -> Option<MethodIdentifier> {
        if !self.end() {
            if let Token::MethodIdentifier(id) = self.get().unwrap() {
                let ident = id.clone();
                self.step();
                return Some(ident);
            }
        }
        None
    }

    pub fn accept_number_literal(&mut self) -> Option<i64> {
        if !self.end() {
            if let Token::NumberLiteral(n) = self.get().unwrap() {
                let number = *n;
                self.step();
                return Some(number);
            }
        }
        None
    }

    pub fn accept_bool_literal(&mut self) -> Option<bool> {
        if !self.end() {
            let maybe_bool = match self.get().unwrap() {
                Token::True => Some(true),
                Token::False => Some(false),
                _ => None,
            };
            if maybe_bool.is_some() {
                self.step();
                return maybe_bool;
            }
        }
        None
    }

    pub fn accept_type(&mut self) -> Option<Type> {
        if !self.end() {
            if let Token::TypeIdentifier(ty) = self.get().unwrap() {
                let type_var = ty.clone();
                self.step();
                return Some(type_var);
            }
        }
        None
    }

    pub fn expect(&mut self, token: &Token) -> Result<()> {
        if self.accept(token) {
            Ok(())
        } else {
            Err(Error::new(
                ErrorKind::InvalidInput,
                format!["expected {:?}", token],
            ))
        }
    }

    pub fn expect_type(&mut self) -> Result<Type> {
        if let Some(ty) = self.accept_type() {
            Ok(ty)
        } else {
            Err(invalid_input("expected typename"))
        }
    }
}
