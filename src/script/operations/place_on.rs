use super::*;

fn execute(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(entity),
            Value::Int3(position)) = expect_2_args(args)? {
        if !world.exists::<Position>(entity) {
            let event = PlaceEvent { position };
            let pack = EventPack::new(entity, event.into());
            Ok(Value::EventPack(pack))
        } else {
            Err(invalid_value("expected entity without Position as first argument of 'place on'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'place on'"))
    }
}

pub static PLACE_ON: Operation = execute;
