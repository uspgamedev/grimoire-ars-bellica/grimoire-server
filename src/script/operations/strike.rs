use super::*;

fn execute(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(target_entity),
            Value::Entity(with_entity),
            Value::Entity(weapon_entity)) = expect_3_args(args)? {
        if !world.exists::<Manipulator>(with_entity) {
            Err(invalid_value("expected Manipulator entity as second argument of 'strike with using'"))
        } else if !world.exists::<Weapon>(weapon_entity) {
            Err(invalid_value("expected Weapon entity as third argument of 'strike with using'"))
        } else {
            let event = DirectStrikeEvent { target_entity, weapon_entity };
            let pack = EventPack::new(with_entity, event.into());
            Ok(Value::EventPack(pack))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'strike with using'"))
    }
}

pub static STRIKE: Operation = execute;
