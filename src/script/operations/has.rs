
use super::*;

fn execute(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(id), Value::String(typename)) = expect_2_args(args)? {
        let check = match typename.as_str() {
            "Applicable" => Value::Boolean(world.exists::<Applicable>(id)),
            "Attacker" => Value::Boolean(world.exists::<Attacker>(id)),
            "Position" => Value::Boolean(world.exists::<Position>(id)),
            "Carryable" => Value::Boolean(world.exists::<Carryable>(id)),
            "Container" => Value::Boolean(world.exists::<Container>(id)),
            "Damageable" => Value::Boolean(world.exists::<Damageable>(id)),
            "Interactive" => Value::Boolean(world.exists::<Interactive>(id)),
            "Manipulator" => Value::Boolean(world.exists::<Manipulator>(id)),
            "Obstacle" => Value::Boolean(world.exists::<Obstacle>(id)),
            "Openable" => Value::Boolean(world.exists::<Openable>(id)),
            "Owned" => Value::Boolean(world.exists::<Owned>(id)),
            "Visible" => Value::Boolean(world.exists::<Visible>(id)),
            "Weapon" => Value::Boolean(world.exists::<Weapon>(id)),
            _ => Value::Boolean(false),
        };
        Ok(check)
    } else {
        Err(invalid_value("unexpected arguments to 'has'"))
    }
}

pub static HAS: Operation = execute;
