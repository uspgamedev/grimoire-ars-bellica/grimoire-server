use super::*;

pub fn execute(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(id),
            Value::Direction(dir)) = expect_2_args(args)? {
        if world.exists::<Position>(id) {
            let vec = Int3::from(dir);
            let event = MoveEvent::Displacement(vec);
            let pack = EventPack::new(id, event.into());
            Ok(Value::EventPack(pack))
        } else {
            Err(invalid_value("expected Position entity as first argument of 'move along'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'move along'"))
    }
}
