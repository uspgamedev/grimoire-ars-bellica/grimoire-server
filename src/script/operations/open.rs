use super::*;

fn execute(_world: &World, args: Value) -> Result<Value> {
    if let Value::Entity(id) = expect_1_arg(args)? {
        let event = OpenEvent{};
        let pack = EventPack::new(id, event.into());
        Ok(Value::EventPack(pack))
    } else {
        Err(invalid_value("unexpected arguments to 'open'"))
    }
}

pub static OPEN: Operation = execute;
