use super::*;
use crate::model::components::carryable::is_carried_by;
use crate::model::components::owned::get_owner;
use crate::model::components::position::get_entity_position;

fn get_position(world: &World, args: Value) -> Result<Value> {
    if let Value::Entity(entity) = expect_1_arg(args)? {
        if let Some(position) = get_entity_position(world, entity) {
            Ok(Value::Int3(position))
        } else {
            Err(invalid_value("entity from first argument of 'get position' has no position"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'get position'"))
    }
}

fn get_manipulator_skill(world: &World, args: Value) -> Result<Value> {
    if let Value::Entity(id) = expect_1_arg(args)? {
        if let Some(manipulator) = world.get::<Manipulator>(id) {
            Ok(Value::Integer(manipulator.get_skill() as i64))
        } else {
            Err(invalid_value("expected Manipulator entity as first argument of 'manipulator skill'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'manipulator skill'"))
    }
}

fn get_manipulator_reach(world: &World, args: Value) -> Result<Value> {
    if let Value::Entity(id) = expect_1_arg(args)? {
        if let Some(manipulator) = world.get::<Manipulator>(id) {
            Ok(Value::Integer(manipulator.get_reach() as i64))
        } else {
            Err(invalid_value("expected Manipulator entity as first argument of 'manipulator reach'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'manipulator reach'"))
    }
}

fn get_wielded_entity(world: &World, args: Value) -> Result<Value> {
    if let Value::Entity(id) = expect_1_arg(args)? {
        if let Some(manipulator) = world.get::<Manipulator>(id) {
            if let Some(wielded) = manipulator.carried_entity {
                Ok(Value::Entity(wielded))
            } else {
                Ok(Value::Boolean(false))
            }
        } else {
            Err(invalid_value("expected Manipulator entity as first argument of 'wielded entity'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'wielded entity'"))
    }
}

fn get_currently_open(world: &World, args: Value) -> Result<Value> {
    if let Value::Entity(id) = expect_1_arg(args)? {
        if let Some(openable) = world.get::<Openable>(id) {
            Ok(Value::Boolean(openable.open))
        } else {
            Err(invalid_value("expected Openable entity as first argument of 'currently open'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'currently open'"))
    }
}

fn get_currently_closed(world: &World, args: Value) -> Result<Value> {
    if let Value::Entity(id) = expect_1_arg(args)? {
        if let Some(openable) = world.get::<Openable>(id) {
            Ok(Value::Boolean(!openable.open))
        } else {
            Err(invalid_value("expected Openable entity as first argument of 'currently closed'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'currently closed'"))
    }
}

fn get_owned_by(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(owned_entity),
            Value::Entity(owner_entity)) = expect_2_args(args)? {
        Ok(Value::Boolean(owner_entity == get_owner(world, owned_entity)))
    } else {
        Err(invalid_value("unexpected arguments to 'owned by'"))
    }
}

fn get_carried_by(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(carried_entity),
            Value::Entity(carrier_entity)) = expect_2_args(args)? {
        Ok(Value::Boolean(is_carried_by(world, carried_entity, carrier_entity)))
    } else {
        Err(invalid_value("unexpected arguments to 'carried by'"))
    }
}

pub static POSITION: Operation = get_position;
pub static MANIPULATOR_REACH: Operation = get_manipulator_reach;
pub static MANIPULATOR_SKILL: Operation = get_manipulator_skill;
pub static WIELDED_ENTITY: Operation = get_wielded_entity;
pub static CURRENTLY_OPEN: Operation = get_currently_open;
pub static CURRENTLY_CLOSED: Operation = get_currently_closed;
pub static OWNED_BY: Operation = get_owned_by;
pub static CARRIED_BY: Operation = get_carried_by;

