use super::*;
use crate::model::components::position::get_entity_position;

fn execute(world: &World, args: Value) -> Result<Value> {
    match expect_3_args(args)? {
        (Value::Int3(pos),
         Value::Integer(distance),
         Value::Entity(referential_entity)) => {
            if let Some(referential) = get_entity_position(world, referential_entity) {
                let diff = pos - referential;
                Ok(Value::Boolean(diff.dot(&diff) <= distance * distance))
            } else {
                Err(invalid_value("expected Position entity as third argument of 'within of'"))
            }
        },
        _ => Err(invalid_value("unexpected arguments to 'within of'")),
    }
}

pub static WITHIN_OF: Operation = execute;
