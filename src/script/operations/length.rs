
use super::*;
use common::intn::Int3;

fn execute(_: &World, args: Value) -> Result<Value> {
    if let Value::Direction(dir) = expect_1_arg(args)? {
        let vec = Int3::from(dir);
        Ok(Value::Float((vec.dot(&vec) as f64).sqrt()))
    } else {
        Err(invalid_value("unexpected arguments to 'length'"))
    }
}

pub static LENGTH: Operation = execute;
