
use super::*;

pub static SAME_AS: Operation = execute_same_as;
pub static OR: Operation = execute_or;
pub static PLUS: Operation = execute_plus;
pub static TIMES: Operation = execute_times;
pub static OVER: Operation = execute_over;

fn execute_same_as(_: &World, args: Value) -> Result<Value> {
    let (obj1, obj2) = expect_2_args(args.clone())?;
    Ok(Value::Boolean(obj1 == obj2))
}

fn execute_or(_: &World, args: Value) -> Result<Value> {
    if let (Value::Boolean(b1), Value::Boolean(b2)) = expect_2_args(args.clone())? {
        Ok(Value::Boolean(b1 || b2))
    } else {
        Err(invalid_value(&format!["unexpected arguments to 'or': {:?}", args]))
    }
}

fn execute_plus(_: &World, args: Value) -> Result<Value> {
    match expect_2_args(args.clone())? {
        (Value::Integer(lhs), Value::Integer(rhs)) => Ok(Value::Integer(lhs + rhs)),
        (Value::Integer(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs as f64 + rhs)),
        (Value::Float(lhs), Value::Integer(rhs)) => Ok(Value::Float(lhs + rhs as f64)),
        (Value::Float(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs + rhs)),
        _ => Err(invalid_value(&format!["unexpected arguments to 'plus': {:?}", args])),
    }
}

fn execute_times(_: &World, args: Value) -> Result<Value> {
    match expect_2_args(args.clone())? {
        (Value::Integer(lhs), Value::Integer(rhs)) => Ok(Value::Integer(lhs * rhs)),
        (Value::Integer(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs as f64 * rhs)),
        (Value::Float(lhs), Value::Integer(rhs)) => Ok(Value::Float(lhs * rhs as f64)),
        (Value::Float(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs * rhs)),
        _ => Err(invalid_value(&format!["unexpected arguments to 'times': {:?}", args])),
    }
}

fn execute_over(_: &World, args: Value) -> Result<Value> {
    match expect_2_args(args.clone())? {
        (Value::Integer(lhs), Value::Integer(rhs)) => Ok(Value::Integer(lhs / rhs)),
        (Value::Integer(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs as f64 / rhs)),
        (Value::Float(lhs), Value::Integer(rhs)) => Ok(Value::Float(lhs / rhs as f64)),
        (Value::Float(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs / rhs)),
        _ => Err(invalid_value(&format!["unexpected arguments to 'over': {:?}", args])),
    }
}
