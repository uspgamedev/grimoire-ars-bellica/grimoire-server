use super::*;
use crate::model::component::Component;

pub type Operation = fn(&World, Value) -> Result<Value>;

pub fn expect_1_arg(args: Value) -> Result<Value> {
    if let Value::Array(arr) = args {
        if arr.len() >= 1 {
            return Ok(arr[0].clone());
        }
    }
    Err(invalid_value("expected at least 1 argument"))
}

pub fn expect_2_args(args: Value) -> Result<(Value, Value)> {
    if let Value::Array(arr) = args {
        if arr.len() >= 2 {
            return Ok((arr[0].clone(), arr[1].clone()));
        }
    }
    Err(invalid_value("expected at least 2 arguments"))
}

pub fn expect_3_args(args: Value) -> Result<(Value, Value, Value)> {
    if let Value::Array(arr) = args {
        if arr.len() >= 3 {
            return Ok((arr[0].clone(), arr[1].clone(), arr[2].clone()));
        }
    }
    Err(invalid_value("expected at least 3 arguments"))
}

pub fn expect_float(value: Value) -> Result<f64> {
    if let Value::Integer(i) = value {
        Ok(i as f64)
    } else if let Value::Float(f) = value {
        Ok(f)
    } else {
        Err(invalid_value("expected float-compatible argument"))
    }
}

pub fn expect_int3(value: Value) -> Result<Int3> {
    if let Value::Direction(dir) = value {
        Ok(Int3::from(dir))
    } else if let Value::Int3(i3) = value {
        Ok(i3)
    } else {
        Err(invalid_value("expected Int3 argument"))
    }
}

pub fn expect_component<T: Component>(world: &World, id: ID) -> Result<&T> {
    if let Some(comp) = world.get::<T>(id) {
        Ok(comp)
    } else {
        Err(invalid_value(&format![
            "{} has no {}",
            id,
            stringify![T]
        ]))
    }
}

pub fn invalid_value(reason: &str) -> Error {
    Error::new(ErrorKind::InvalidData, reason)
}

pub fn unexpected_arguments_to_method(method_name: &'static str) -> Result<Value> {
    Err(invalid_value(&format![
        "unexpected arguments to {}",
        method_name
    ]))
}
