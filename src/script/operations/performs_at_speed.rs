use super::*;
use crate::model::components::actor::*;

fn execute(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(id),
            Value::EventPack(pack),
            percent_obj) = expect_3_args(args)? {
        if world.exists::<Actor>(id) {
            let percent = expect_float(percent_obj)?;
            let factor = percent / 100.0;
            let cost = (MAX_ENERGY as f64 / factor) as usize;
            let event = ActEvent {
                acts: vec![Act::new(cost, pack)]
            };
            let act_pack = EventPack::new(id, event.into());
            Ok(Value::EventPack(act_pack))
        } else {
            Err(invalid_value("expected Actor entity as first argument of 'perform at speed'"))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'performs at speed'"))
    }
}

pub static PERFORMS_AT_SPEED: Operation = execute;
