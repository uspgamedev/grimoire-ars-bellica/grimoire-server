use super::*;

fn execute(world: &World, args: Value) -> Result<Value> {
    if let (Value::Entity(stored_entity),
            Value::Entity(container_entity)) = expect_2_args(args)? {
        if !world.exists::<Carryable>(stored_entity) {
            Err(invalid_value("expected Carryable entity as first argument of 'store inside'"))
        } else if !world.exists::<Container>(container_entity) {
            Err(invalid_value("expected Container entity as second argument of 'store inside'"))
        } else {
            let event = StoreEvent{ stored_entity };
            let pack = EventPack::new(container_entity, event.into());
            Ok(Value::EventPack(pack))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'store inside'"))
    }
}

pub static STORE_INSIDE: Operation = execute;
