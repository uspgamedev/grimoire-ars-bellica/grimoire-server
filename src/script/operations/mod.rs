pub use crate::model::world::World;
pub use crate::core::*;
pub use crate::script::interpreter::PreparationInterpreter;
pub use common::appearance::Appearance;
pub use common::directions::Direction;
pub use common::identity::ID;
pub use common::intn::*;
pub use std::collections::HashMap;
pub use std::convert::identity as id;
pub use std::io::{Error, ErrorKind, Result};

use crate::model::components::*;
use crate::model::event::all::*;
use crate::model::event::EventPack;

pub mod operation;
pub use operation::*;

mod close;
mod getters;
mod has;
mod length;
mod move_along;
mod open;
mod operands;
mod performs_at_speed;
mod place_on;
mod rest;
mod store_inside;
mod strike;
mod within_of;

lazy_static! {
    // TODO: separate into 2 tables
    pub static ref OP_TABLE: HashMap<&'static str, Operation> = {
        let mut table = HashMap::default();
        table.insert("+", operands::PLUS);
        table.insert("/", operands::OVER);
        table.insert("x", operands::TIMES);
        table.insert("*", operands::TIMES);
        table.insert("same_as", operands::SAME_AS);
        table.insert("or", operands::OR);
        table.insert("position", getters::POSITION);
        table.insert("manipulator_reach", getters::MANIPULATOR_REACH);
        table.insert("manipulator_skill", getters::MANIPULATOR_SKILL);
        table.insert("wielded_entity", getters::WIELDED_ENTITY);
        table.insert("currently_open", getters::CURRENTLY_OPEN);
        table.insert("currently_closed", getters::CURRENTLY_CLOSED);
        table.insert("owned_by", getters::OWNED_BY);
        table.insert("carried_by", getters::CARRIED_BY);
        table.insert("close", close::CLOSE);
        table.insert("has", has::HAS);
        table.insert("length", length::LENGTH);
        table.insert("move_along", move_along::execute);
        table.insert("open", open::OPEN);
        table.insert("performs_at_speed", performs_at_speed::PERFORMS_AT_SPEED);
        table.insert("place_on", place_on::PLACE_ON);
        table.insert("rest", rest::REST);
        table.insert("store_inside", store_inside::STORE_INSIDE);
        table.insert("strike_with_using", strike::STRIKE);
        table.insert("within_of", within_of::WITHIN_OF);
        table
    };
}
