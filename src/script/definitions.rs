
use super::interpreter::not_found;

use std::collections::HashMap;
use std::io::Result;

#[derive(PartialEq)]
pub enum ScriptSection {
    Declarations,
    Executions,
}

pub type DeclarationTable = HashMap<ValueIdentifier, Type>;
pub type ValueIdentifier = String;
pub type MethodIdentifier = String;

#[derive(Debug, PartialEq)]
pub struct Assignment {
    pub identifier: ValueIdentifier,
    pub expr: Box<Expression>,
}

#[derive(Debug, PartialEq)]
pub enum Expression {
    Assignment(Assignment),
    Call(Call),
    Value(Value),
    IfStatement(IfStatement),
}

#[derive(Debug, PartialEq)]
pub enum IfStatement {
    IfThen(Box<Expression>, Box<Expression>),
    IfThenElse(Box<Expression>, Box<Expression>, Box<Expression>),
}

#[derive(Debug, PartialEq)]
pub struct Call {
    pub maybe_first_value: Option<Box<Value>>,
    pub body: CallBody,
}

#[derive(Debug, PartialEq)]
pub struct CallBody {
    pub method_ident: MethodIdentifier,
    pub args: Vec<Value>,
    pub tail: Option<Box<CallBody>>,
}

#[derive(Debug, PartialEq)]
pub enum Value {
    Identifier(ValueIdentifier),
    Type(Type),
    IntegerLiteral(i64),
    FloatLiteral(f64),
    BoolLiteral(bool),
    BracketExpression(Box<Expression>),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Type {
    ID,
    Direction,
    Int,
    Int3,
    Attacker,
    Position,
    Carryable,
    Container,
    Damageable,
    Manipulator,
    Openable,
    Visible,
    Weapon,
}

pub struct TypeTable<'a> {
    type_map: HashMap<&'a str, Type>,
}

impl<'a> Default for TypeTable<'a> {
    fn default() -> Self {
        let mut type_map = HashMap::default();
        type_map.insert("ID", Type::ID);
        type_map.insert("Direction", Type::Direction);
        type_map.insert("Attacker", Type::Attacker);
        type_map.insert("Position", Type::Position);
        type_map.insert("Carryable", Type::Carryable);
        type_map.insert("Container", Type::Container);
        type_map.insert("Damageable", Type::Damageable);
        type_map.insert("Manipulator", Type::Manipulator);
        type_map.insert("Openable", Type::Openable);
        type_map.insert("Visible", Type::Visible);
        type_map.insert("Weapon", Type::Weapon);
        type_map.insert("Int", Type::Int);
        type_map.insert("Int3", Type::Int3);
        Self { type_map }
    }
}

impl<'a> TypeTable<'a> {
    pub fn get(&self, key: &str) -> Option<&Type> {
        self.type_map.get(key)
    }

    pub fn get_string_from(&self, ty: &Type) -> Result<String> {
        for (str_key, type_value) in &self.type_map {
            if type_value == ty {
                return Ok(String::from(*str_key));
            }
        }
        Err(not_found(&format!["Unrecognized type {:?}", ty]))
    }
}

lazy_static! {
    pub static ref TYPE_TABLE: TypeTable<'static> = TypeTable::default();
}
