use std::collections::HashMap;

use crate::model::action::ActionDB;
use super::action::ScriptAction;
use super::definitions::*;
use super::token_stream::TokenStream;
use super::tokenizer::{tokenize_file, Token};

use std::fs;
use std::io::{Error, ErrorKind, Result};

pub fn parse(action_db: &mut ActionDB) -> Result<()> {
    let mut action_scripts: HashMap<String, ScriptAction> = HashMap::default();
    for entry in fs::read_dir("database/actions")? {
        let entry_file = entry?;
        let script_name = entry_file
            .file_name()
            .into_string()
            .unwrap()
            .split('.')
            .next()
            .unwrap()
            .to_string();
        let token_lines = tokenize_file(&entry_file.path())?;
        let mut declarations = Vec::default();
        let mut executions = Vec::default();
        let mut section = ScriptSection::Declarations;
        println!["Tokens\n{:?}\n", token_lines];
        for token_list in token_lines {
            if !token_list.is_empty() {
                if token_list.len() == 1 && token_list[0] == Token::Separator {
                    section = match section {
                        ScriptSection::Declarations => ScriptSection::Executions,
                        _ => {
                            return Err(Error::new(
                                ErrorKind::InvalidInput,
                                "Script file with more than 2 sections!",
                            ))
                        }
                    }
                } else {
                    let vec = match section {
                        ScriptSection::Declarations => &mut declarations,
                        ScriptSection::Executions => &mut executions,
                    };
                    vec.push(token_list);
                }
            }
        }
        let declaration_table = parse_declarations(declarations);
        let parsed_executions = parse_executions(executions)?;
        println!["Executions\n{:?}\n", parsed_executions];
        let action_script = ScriptAction {
            declaration_table,
            script: parsed_executions,
        };
        action_scripts.insert(script_name, action_script);
    }
    for (name, script) in action_scripts.into_iter() {
        action_db.insert(name, Box::new(script));
    }
    Ok(())
}

fn parse_declarations(declarations: Vec<Vec<Token>>) -> DeclarationTable {
    let mut declaration_table = HashMap::default();
    for declaration in declarations {
        match declaration[0] {
            Token::ValueIdentifier(ref identifier) => {
                if declaration[1] == Token::Colon {
                    match declaration[2] {
                        Token::TypeIdentifier(ref ty) => {
                            declaration_table.insert(identifier.clone(), ty.clone());
                        }
                        _ => panic!["Invalid Type Token for declaration: {:?}", declaration[2]],
                    }
                } else {
                    panic!["Missing separator token ':' on declaration"];
                }
            }
            _ => panic![
                "Invalid value identifier Token for declaration: {:?}",
                declaration[0]
            ],
        }
    }
    declaration_table
}

pub fn parse_executions(mut executions: Vec<Vec<Token>>) -> Result<Vec<Expression>> {
    let mut parsed_executions = Vec::default();
    for execution in executions.drain(..) {
        let mut token_stream = TokenStream::new(execution);
        let parsed_execution = parse_expr(&mut token_stream)?;
        parsed_executions.push(parsed_execution);
    }
    Ok(parsed_executions)
}

fn parse_expr(token_stream: &mut TokenStream) -> Result<Expression> {
    if token_stream.end() {
        Err(invalid_input("unexpected end of expression"))
    } else if let Some(assignment) = parse_assignment(token_stream)? {
        Ok(assignment)
    } else if let Some(if_statement) = parse_if(token_stream)? {
        Ok(Expression::IfStatement(if_statement))
    } else if let Some(call) = parse_call(token_stream) {
        Ok(Expression::Call(call))
    } else if let Ok(value) = parse_value(token_stream) {
        Ok(Expression::Value(value))
    } else {
        Err(invalid_input(
            format!["Invalid Expression at {:?}", token_stream.get().unwrap()].as_str(),
        ))
    }
}

fn parse_assignment(token_stream: &mut TokenStream) -> Result<Option<Expression>> {
    token_stream.push();
    if let Some(id) = token_stream.accept_value_identifier() {
        if token_stream.accept(&Token::Is) {
            let expr = parse_expr(token_stream)?;
            token_stream.pop();
            return Ok(Some(Expression::Assignment(Assignment {
                identifier: id,
                expr: Box::new(expr),
            })));
        }
    }
    token_stream.pop_and_rollback();
    Ok(None)
}

fn parse_if(token_stream: &mut TokenStream) -> Result<Option<IfStatement>> {
    if token_stream.accept(&Token::If) {
        let expr1 = parse_expr(token_stream)?;
        if token_stream.accept(&Token::Then) {
            let expr2 = parse_expr(token_stream)?;
            if token_stream.accept(&Token::Else) {
                let expr3 = parse_expr(token_stream)?;
                Ok(Some(IfStatement::IfThenElse(
                    Box::new(expr1),
                    Box::new(expr2),
                    Box::new(expr3),
                )))
            } else {
                Ok(Some(IfStatement::IfThen(Box::new(expr1), Box::new(expr2))))
            }
        } else {
            Err(invalid_input("missing 'Then' after 'if'"))
        }
    } else {
        Ok(None)
    }
}

fn parse_call(token_stream: &mut TokenStream) -> Option<Call> {
    token_stream.push();
    match token_stream.get() {
        Some(Token::MethodIdentifier(_)) => match parse_call_body(token_stream) {
            Some(body) => {
                token_stream.pop();
                Some(Call {
                    maybe_first_value: None,
                    body,
                })
            },
            None => {
                token_stream.pop_and_rollback();
                None
            }
        },
        _ => {
            if let Ok(value) = parse_value(token_stream) {
                let maybe_first_value = Some(Box::new(value));
                match parse_call_body(token_stream) {
                    Some(body) => {
                        token_stream.pop();
                        Some(Call {
                            maybe_first_value,
                            body,
                        })
                    },
                    None => {
                        token_stream.pop_and_rollback();
                        None
                    }
                }
            } else {
                token_stream.pop_and_rollback();
                None
            }
        }
    }
}

fn parse_call_body(token_stream: &mut TokenStream) -> Option<CallBody> {
    if let Some(method_ident) = token_stream.accept_method_identifier() {
        let mut args = Vec::default();
        while let Ok(value) = parse_value(token_stream) {
            args.push(value);
        }
        let tail = if let Some(tail) = parse_call_body(token_stream) {
            Some(Box::new(tail))
        } else {
            None
        };
        Some(CallBody {
            method_ident,
            args,
            tail,
        })
    } else {
        None
    }
}

fn parse_value(token_stream: &mut TokenStream) -> Result<Value> {
    if token_stream.end() {
        Err(invalid_input("unexpected end of value"))
    } else if let Some(id) = token_stream.accept_value_identifier() {
        Ok(Value::Identifier(id))
    } else if let Some(ty) = token_stream.accept_type() {
        Ok(Value::Type(ty))
    } else if let Some(n) = token_stream.accept_number_literal() {
        Ok(Value::IntegerLiteral(n))
    } else if let Some(b) = token_stream.accept_bool_literal() {
        Ok(Value::BoolLiteral(b))
    } else if token_stream.accept(&Token::LBracket) {
        let expr = parse_expr(token_stream)?;
        if token_stream.accept(&Token::RBracket) {
            Ok(Value::BracketExpression(Box::new(expr)))
        } else {
            println!("-> {:?}", token_stream);
            Err(invalid_input("missing ']' at end of expression"))
        }
    } else {
        Err(invalid_input(
            format!["unexpected token {:?}", token_stream.get().unwrap()].as_str(),
        ))
    }
}

pub fn invalid_input(reason: &str) -> Error {
    Error::new(ErrorKind::InvalidInput, reason)
}

/*
 *  script = declarations sep executions
 *
 *  declarations = declaration [endl declaration]
 *  declaration = IDENTIFIER ":" TYPE
 *
 *  executions = execution [endl execution]
 *  execution = expr
 *
 *  expr = assignment
 *       | if_statement
 *       | call
 *       | value
 *
 *  call = expr? call_body
 *  call_body = METHOD_IDENTIFIER [ value ] call_body?
 *
 *  assignment = VALUE_IDENTIFIER "is" expr
 *
 *  value = VALUE_IDENTIFIER
 *        | TYPE_IDENTIFIER
 *        | NUMBER_LITERAL
 *        | BOOL_LITERAL
 *        | tuple
 *        | "[" expr "]"
 *
 *  tuple = "(" expr [ "," expr ] ")"
 *
 *  METHOD_IDENTIFIER = [a-z]+
 *  VALUE_IDENTIFIER = [A-Z][a-z]*
 *  if_statement = "if" expr "then" expr
 *               | "if" expr "then" expr "else" expr
 *
 *  ---
 *
 *  [ value ] METHOD_NAME
 *  Foo * Bar
 *
 *  #[derive(ScriptMethod)]
 *  fn openable_is_open(world: &World, args: TomlValue) -> TomlValue {
 *      ...
 *  }
 *
 */
