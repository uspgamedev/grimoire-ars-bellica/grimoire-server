use crate::model::components::attacker::Attacker;
use crate::model::components::body::{get_entity_ids_at, Body};
use crate::model::components::carryable::Carryable;
use crate::model::components::container::Container;
use crate::model::components::damageable::Damageable;
use crate::model::components::manipulator::Manipulator;
use crate::model::components::openable::Openable;
use crate::model::components::visible::Visible;
use crate::model::components::weapon::Weapon;
use crate::model::world::World;
use common::appearance::Appearance;
use common::directions::Direction;
use common::identity::ID;
use common::intn::*;
use std::collections::HashMap;
use std::convert::identity as id;
use std::io::{Error, ErrorKind, Result};
use toml::Value;

fn i64_to_usize(i: i64) -> usize {
    i as usize
}

macro_rules! get_mut {
    ($world:expr, $component:ty, $id:expr) => {
        match $world.get_handle::<$component>($id as ID) {
            Some(comp) => comp,
            None => {
                return Err(invalid_value(&format![
                    "entity doesn't have {}",
                    stringify![$component]
                ]))
            }
        }
    };
}

macro_rules! get {
    ($world:expr, $component:ty, $id:expr) => {
        match $world.get::<$component>($id as ID) {
            Some(comp) => comp,
            None => {
                return Err(invalid_value(&format![
                    "entity doesn't have {}",
                    stringify![$component]
                ]))
            }
        }
    };
}

macro_rules! getter_access {
    ($obj:expr, $attr:ident) => {
        $obj.$attr()
    };
}

macro_rules! read_access {
    ($obj:expr, $attr:ident) => {
        $obj.$attr
    };
}

macro_rules! get_attribute {
    ($world:expr, $component:ty, $id:expr, $getter:ident, $access:ident) => {
        $access![get![$world, $component, $id], $getter]
    };
    ($world:expr, $component:ty, $id:expr, $getter:ident, $access:ident, $ret_type:ty) => {
        $access![get![$world, $component, $id], $getter] as $ret_type
    };
}

macro_rules! wrap_getter {
    ($opname:ident, $component:ty, $getter:ident, $method:ident) => {
        fn $opname(world: &mut World, args: Value) -> Result<Value> {
            if let Value::Integer(id_int) = expect_1_arg(args)? {
                Ok(get_attribute![world, $component, id_int, $getter, $method]
                    .clone()
                    .into())
            } else {
                unexpected_arguments_to_method(stringify![$opname])
            }
        }
    };
    ($opname:ident, $component:ty, $getter:ident, $method:ident, i64) => {
        fn $opname(world: &mut World, args: Value) -> Result<Value> {
            if let Value::Integer(id_int) = expect_1_arg(args)? {
                Ok(Value::Integer(get_attribute![
                    world, $component, id_int, $getter, $method, i64
                ]))
            } else {
                unexpected_arguments_to_method(stringify![$opname])
            }
        }
    };
    ($opname:ident, $component:ty, $getter:ident, $method:ident, bool) => {
        fn $opname(world: &mut World, args: Value) -> Result<Value> {
            if let Value::Integer(id_int) = expect_1_arg(args)? {
                Ok(Value::Boolean(get_attribute![
                    world, $component, id_int, $getter, $method, bool
                ]))
            } else {
                unexpected_arguments_to_method(stringify![$opname])
            }
        }
    };
}

macro_rules! wrap_optional_getter {
    ($opname:ident, $component:ty, $getter:ident, $method:ident, i64) => {
        fn $opname(world: &mut World, args: Value) -> Result<Value> {
            if let Value::Integer(id_int) = expect_1_arg(args)? {
                match get_attribute![world, $component, id_int, $getter, $method] {
                    Some(i) => Ok(Value::Integer(i as i64)),
                    None => Err(invalid_value(&format![
                        "attempt to get {} from {} returned None",
                        stringify![$getter],
                        stringify![$component]
                    ])),
                }
            } else {
                unexpected_arguments_to_method(stringify![$opname])
            }
        }
    };
}

macro_rules! wrap_setter {
    ($opname:ident, $component:ty, $setter:ident, $value_type:ident, $convert:expr) => {
        fn $opname(world: &mut World, args: Value) -> Result<Value> {
            if let (Value::Integer(id_int), Value::$value_type(value)) = expect_2_args(args)? {
                get_mut![world, $component, id_int].$setter($convert(value));
                Ok(Value::Boolean(true))
            } else {
                unexpected_arguments_to_method(stringify![$opname])
            }
        }
    };
}

pub type Operation = fn(&mut World, Value) -> Result<Value>;

fn identity(_: &mut World, args: Value) -> Result<Value> {
    Ok(args)
}

fn has(world: &mut World, args: Value) -> Result<Value> {
    if let (Value::Integer(id_int), Value::String(typename)) = expect_2_args(args)? {
        let id = id_int as ID;
        let check = match typename.as_str() {
            "Attacker" => Value::Boolean(world.exists::<Attacker>(id)),
            "Body" => Value::Boolean(world.exists::<Body>(id)),
            "Carryable" => Value::Boolean(world.exists::<Carryable>(id)),
            "Container" => Value::Boolean(world.exists::<Container>(id)),
            "Damageable" => Value::Boolean(world.exists::<Damageable>(id)),
            "Manipulator" => Value::Boolean(world.exists::<Manipulator>(id)),
            "Openable" => Value::Boolean(world.exists::<Openable>(id)),
            "Visible" => Value::Boolean(world.exists::<Visible>(id)),
            "Weapon" => Value::Boolean(world.exists::<Weapon>(id)),
            _ => Value::Boolean(false),
        };
        Ok(check)
    } else {
        Err(invalid_value("unexpected arguments to 'has'"))
    }
}

fn or(_: &mut World, args: Value) -> Result<Value> {
    if let (Value::Boolean(lhs), Value::Boolean(rhs)) = expect_2_args(args)? {
        Ok(Value::Boolean(lhs || rhs))
    } else {
        Err(invalid_value("unexpected arguments to 'or'"))
    }
}

fn same_as(_: &mut World, args: Value) -> Result<Value> {
    if let (Value::Integer(lhs_id), Value::Integer(rhs_id)) = expect_2_args(args)? {
        Ok(Value::Boolean(lhs_id == rhs_id))
    } else {
        Err(invalid_value("unexpected arguments to 'same as'"))
    }
}

fn plus(_: &mut World, args: Value) -> Result<Value> {
    match expect_2_args(args)? {
        (Value::Integer(lhs), Value::Integer(rhs)) => Ok(Value::Integer(lhs + rhs)),
        (Value::Integer(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs as f64 + rhs)),
        (Value::Float(lhs), Value::Integer(rhs)) => Ok(Value::Float(lhs + rhs as f64)),
        (Value::Float(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs + rhs)),
        _ => Err(invalid_value("unexpected arguments to 'plus'")),
    }
}

fn times(_: &mut World, args: Value) -> Result<Value> {
    match expect_2_args(args)? {
        (Value::Integer(lhs), Value::Integer(rhs)) => Ok(Value::Integer(lhs * rhs)),
        (Value::Integer(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs as f64 * rhs)),
        (Value::Float(lhs), Value::Integer(rhs)) => Ok(Value::Float(lhs * rhs as f64)),
        (Value::Float(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs * rhs)),
        _ => Err(invalid_value("unexpected arguments to 'times'")),
    }
}

fn over(_: &mut World, args: Value) -> Result<Value> {
    match expect_2_args(args)? {
        (Value::Integer(lhs), Value::Integer(rhs)) => Ok(Value::Integer(lhs / rhs)),
        (Value::Integer(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs as f64 / rhs)),
        (Value::Float(lhs), Value::Integer(rhs)) => Ok(Value::Float(lhs / rhs as f64)),
        (Value::Float(lhs), Value::Float(rhs)) => Ok(Value::Float(lhs / rhs)),
        _ => Err(invalid_value("unexpected arguments to 'over'")),
    }
}

fn near_by(world: &mut World, args: Value) -> Result<Value> {
    if let (Value::Integer(id1_int), Value::Integer(id2_int), Value::Integer(reach)) =
        expect_3_args(args)?
    {
        let body1 = world.get::<Body>(id1_int as ID).unwrap();
        let body2 = world.get::<Body>(id2_int as ID).unwrap();
        let distance = (body1.get_position() - body2.get_position()).size() as i64;
        Ok(Value::Boolean(distance <= reach))
    } else {
        Err(invalid_value("unexpected arguments to 'nearby by'"))
    }
}

fn length(_: &mut World, args: Value) -> Result<Value> {
    let arg = expect_1_arg(args)?;
    let vec = expect_int3(arg)?;
    Ok(Value::Float((vec.dot(&vec) as f64).sqrt()))
}

fn move_along(world: &mut World, args: Value) -> Result<Value> {
    if let (Value::Integer(id_int), dir) = expect_2_args(args)? {
        let vec = expect_int3(dir)?;
        world
            .get_handle::<Body>(id_int as ID)
            .unwrap()
            .move_along(&vec);
        Ok(Value::Boolean(true))
    } else {
        Err(invalid_value("unexpected arguments to 'move along'"))
    }
}

fn add_to(world: &mut World, args: Value) -> Result<Value> {
    if let (Value::Integer(item_id_int), Value::Integer(container_id_int)) = expect_2_args(args)? {
        world
            .get_handle::<Container>(container_id_int as ID)
            .unwrap()
            .add_item(item_id_int as ID);
        Ok(Value::Boolean(true))
    } else {
        Err(invalid_value("unexpected arguments to 'add_to'"))
    }
}

wrap_getter![reach, Manipulator, get_reach, getter_access, i64];
wrap_getter![position, Body, get_position, getter_access];
wrap_setter![set_passable, Body, set_passable, Boolean, id];
wrap_getter![is_open, Openable, is_open, getter_access, bool];
wrap_getter![appearance, Visible, get_appearance, getter_access, i64];
wrap_setter![
    set_appearance,
    Visible,
    set_appearance,
    Integer,
    Appearance::from
];
wrap_getter![
    openable_appearance,
    Openable,
    get_appearance,
    getter_access,
    i64
];
wrap_getter![attacker_skill, Attacker, get_skill, getter_access, i64];
wrap_optional_getter![
    wielded_entity,
    Manipulator,
    carried_entity,
    read_access,
    i64
];
wrap_getter![attacker_power, Attacker, get_power, getter_access, i64];
wrap_getter![weapon_power, Weapon, power, read_access, i64];
wrap_setter![take_damage, Damageable, take_damage, Integer, i64_to_usize];

fn entities_at(world: &mut World, args: Value) -> Result<Value> {
    let position = expect_int3(expect_1_arg(args)?)?;
    let value_vec: Vec<Value> = get_entity_ids_at(world, &position)
        .into_iter()
        .map(|id| Value::Integer(id as i64))
        .collect();
    Ok(Value::Array(value_vec))
}

fn first_from(world: &mut World, args: Value) -> Result<Value> {
    if let (Value::String(typename), Value::Array(id_list)) = expect_2_args(args)? {
        let maybe_id_value = id_list
            .into_iter()
            .filter(|id_value| match id_value {
                Value::Integer(id_int) => {
                    let id = *id_int as ID;
                    match typename.as_str() {
                        "Attacker" => world.exists::<Attacker>(id),
                        "Body" => world.exists::<Body>(id),
                        "Carryable" => world.exists::<Carryable>(id),
                        "Container" => world.exists::<Container>(id),
                        "Damageable" => world.exists::<Damageable>(id),
                        "Manipulator" => world.exists::<Manipulator>(id),
                        "Openable" => world.exists::<Openable>(id),
                        "Visible" => world.exists::<Visible>(id),
                        "Weapon" => world.exists::<Weapon>(id),
                        _ => false,
                    }
                }
                _ => false,
            })
            .next();
        if let Some(id_value) = maybe_id_value {
            Ok(id_value)
        } else {
            Ok(Value::Boolean(false))
        }
    } else {
        Err(invalid_value("unexpected arguments to 'first_from'"))
    }
}

fn except(_world: &mut World, args: Value) -> Result<Value> {
    if let (Value::Array(list), id_value) = expect_2_args(args)? {
        Ok(Value::Array(
            list.clone()
                .into_iter()
                .filter(|value| *value != id_value)
                .collect(),
        ))
    } else {
        Err(invalid_value("unexpected arguments to 'except'"))
    }
}

fn are_passable(world: &mut World, args: Value) -> Result<Value> {
    if let Value::Array(list) = expect_1_arg(args)? {
        Ok(Value::Boolean(list.iter().all(|value| {
            if let Value::Integer(id_int) = value {
                world
                    .get::<Body>(*id_int as ID)
                    .unwrap()
                    .is_passable()
            } else {
                false
            }
        })))
    } else {
        Err(invalid_value("unexpected arguments to 'are_passable'"))
    }
}

fn toggle(world: &mut World, args: Value) -> Result<Value> {
    if let Value::Integer(toggable_id) = expect_1_arg(args)? {
        world
            .get_mut::<Openable>(toggable_id as ID)
            .unwrap()
            .toggle_open();
        Ok(Value::Boolean(true))
    } else {
        Err(invalid_value("unexpected arguments to 'toggle'"))
    }
}

lazy_static! {
    pub static ref OP_TABLE: HashMap<&'static str, Operation> = {
        let mut table = HashMap::default();
        table.insert("", identity as Operation);
        table.insert("has", has as Operation);
        table.insert("or", or as Operation);
        table.insert("same_as", same_as as Operation);
        table.insert("+", plus as Operation);
        table.insert("*", times as Operation);
        table.insert("/", over as Operation);
        table.insert("near_by", near_by as Operation);
        table.insert("reach", reach as Operation);
        table.insert("length", length as Operation);
        table.insert("move_along", move_along as Operation);
        table.insert("position", position as Operation);
        table.insert("entities_at", entities_at as Operation);
        table.insert("except", except as Operation);
        table.insert("are_passable", are_passable as Operation);
        table.insert("toggle", toggle as Operation);
        table.insert("set_passable", set_passable as Operation);
        table.insert("open?", is_open as Operation);
        table.insert("appearance", appearance as Operation);
        table.insert("set_appearance", set_appearance as Operation);
        table.insert("openable_appearance", openable_appearance as Operation);
        table.insert("attacker_skill", attacker_skill as Operation);
        table.insert("attacker_power", attacker_power as Operation);
        table.insert("wielded_entity", wielded_entity as Operation);
        table.insert("weapon_power", weapon_power as Operation);
        table.insert("take_damage", take_damage as Operation);
        table.insert("first_from", first_from as Operation);
        table.insert("add_to", add_to as Operation);
        table
    };
}

fn expect_1_arg(args: Value) -> Result<Value> {
    if let Value::Array(arr) = args {
        if arr.len() >= 1 {
            return Ok(arr[0].clone());
        }
    }
    Err(invalid_value("expected at least 1 argument"))
}

fn expect_2_args(args: Value) -> Result<(Value, Value)> {
    if let Value::Array(arr) = args {
        if arr.len() >= 2 {
            return Ok((arr[0].clone(), arr[1].clone()));
        }
    }
    Err(invalid_value("expected at least 2 arguments"))
}

fn expect_3_args(args: Value) -> Result<(Value, Value, Value)> {
    if let Value::Array(arr) = args {
        if arr.len() >= 3 {
            return Ok((arr[0].clone(), arr[1].clone(), arr[2].clone()));
        }
    }
    Err(invalid_value("expected at least 3 arguments"))
}

fn expect_int3(value: Value) -> Result<Int3> {
    if let Value::Integer(dir) = value {
        Ok(Int3::from(Direction::from(dir)))
    } else if let Value::Array(arr) = value {
        Ok(Int3::from(arr))
    } else {
        Err(invalid_value("expected Int3 argument"))
    }
}

fn invalid_value(reason: &str) -> Error {
    Error::new(ErrorKind::InvalidData, reason)
}

fn unexpected_arguments_to_method(method_name: &'static str) -> Result<Value> {
    Err(invalid_value(&format![
        "unexpected arguments to {}",
        method_name
    ]))
}
