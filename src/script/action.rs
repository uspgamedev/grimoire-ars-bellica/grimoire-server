use std::io::{Error, ErrorKind, Result};

use crate::core::ValueTable;

use crate::model::event::Effect;
use crate::model::world::World;
use crate::model::action::Action;

use super::definitions::*;
use super::interpreter::*;

#[derive(Debug)]
pub struct ScriptAction {
    pub declaration_table: DeclarationTable,
    pub script: Vec<Expression>,
}

impl Action for ScriptAction {
    fn compile(&self, world: &World, parameters: ValueTable) -> Result<Effect> {
        if validate_declarations(&self.declaration_table, &parameters) {
            let mut interpreter = PreparationInterpreter { world, registers: parameters };
            let mut effect = Effect::default();
            for preparation in self.script.iter() {
                if let Some(pack) = interpreter.interpret_preparation(preparation)? {
                    effect.push(pack);
                }
            }
            Ok(effect)
        } else {
            Err(Error::new(ErrorKind::InvalidData, String::from("bad input")))
        }
    }
}

// TODO: use Result<> for better error handling
fn validate_declarations(declarations: &DeclarationTable, values: &ValueTable) -> bool {
    for (name, ty) in declarations.iter() {
        if let Some(value) = values.get(name) {
            match ty {
                Type::ID => if value.as_id().is_none() { return false; },
                Type::Direction => if value.as_direction().is_none() { return false; },
                Type::Int => if value.as_integer().is_none() { return false; },
                Type::Int3 => if value.as_int3().is_none() { return false; },
                _ => { return false; },
            };
        } else {
            return false;
        }
    }
    true
}
