use std::io::{Error, ErrorKind, Result};

use crate::core::{Value as ObjValue, ValueTable};
use super::definitions::*;
use super::operations::operation::{invalid_value, Operation};
use super::operations::OP_TABLE;
use crate::model::event::EventPack;
use crate::model::world::World;

pub trait Interpreter {
    fn get_registers_mut(&mut self) -> &mut ValueTable;

    fn interpret_operation(
        &mut self,
        operation: &Operation,
        values: ObjValue,
    ) -> Result<ObjValue>;

    fn interpret_expr(&mut self, expression: &Expression) -> Result<ObjValue> {
        match expression {
            Expression::Assignment(assign) => self.interpret_expr_assignment(assign),
            Expression::IfStatement(if_statement) => self.interpret_if(if_statement),
            Expression::Call(call) => self.interpret_call(call),
            Expression::Value(value) => self.interpret_value(value),
        }
    }

    fn interpret_call(&mut self, call: &Call) -> Result<ObjValue> {
        let arg_vec = match &call.maybe_first_value {
            Some(first_value) => vec![self.interpret_value(&first_value)?],
            None => vec![],
        };
        self.interpret_call_body(&call.body, "".to_string(), arg_vec)
    }

    fn interpret_call_body(
        &mut self,
        call_body: &CallBody,
        mut method_name_prefix: String,
        mut args: Vec<ObjValue>,
    ) -> Result<ObjValue> {
        if !method_name_prefix.is_empty() {
            method_name_prefix.push('_');
        }
        method_name_prefix.push_str(call_body.method_ident.as_str());
        for current_args in call_body.args.iter() {
            args.push(self.interpret_value(&current_args)?);
        }
        match &call_body.tail {
            Some(tail) => self.interpret_call_body(&tail, method_name_prefix, args),
            None => {
                if let Some(method) = OP_TABLE.get(method_name_prefix.as_str()) {
                    let values = ObjValue::Array(args);
                    self.interpret_operation(method, values)
                } else {
                    Err(not_found(&format![
                        "Method not found: {:?}",
                        method_name_prefix
                    ]))
                }
            }
        }
    }

    fn interpret_expr_assignment(&mut self, assign: &Assignment) -> Result<ObjValue> {
        let value = self.interpret_expr(&assign.expr)?;
        self.get_registers_mut().insert(assign.identifier.clone(), value.clone());
        Ok(value)
    }

    fn interpret_if(&mut self, if_statement: &IfStatement) -> Result<ObjValue> {
        match if_statement {
            IfStatement::IfThen(expr1, expr2) => {
                if let ObjValue::Boolean(b) = self.interpret_expr(expr1)? {
                    if b {
                        self.interpret_expr(expr2)
                    } else {
                        Ok(ObjValue::Boolean(true)) // keep executing
                    }
                } else {
                    Err(invalid_value(&format![
                        "'If' does not operate on non-boolean values: {:?}",
                        expr1
                    ]))
                }
            }
            IfStatement::IfThenElse(expr1, expr2, expr3) => {
                if let ObjValue::Boolean(b) = self.interpret_expr(expr1)? {
                    if b {
                        self.interpret_expr(expr2)
                    } else {
                        self.interpret_expr(expr3)
                    }
                } else {
                    Err(invalid_value(&format![
                        "'If' does not operate on non-boolean values: {:?}",
                        expr1
                    ]))
                }
            }
        }
    }

    fn interpret_value(&mut self, value: &Value) -> Result<ObjValue> {
        match value {
            Value::Identifier(ident) => match self.get_registers_mut().get(ident) {
                Some(x) => Ok(x.clone()),
                None => Err(not_found(&format!["missing variable {:?}", ident])),
            },
            Value::Type(ty) => Ok(ObjValue::String(TypeTable::default().get_string_from(ty)?)),
            Value::IntegerLiteral(i) => Ok(ObjValue::Integer(*i)),
            Value::FloatLiteral(f) => Ok(ObjValue::Float(*f)),
            Value::BoolLiteral(b) => Ok(ObjValue::Boolean(*b)),
            Value::BracketExpression(expr) => self.interpret_expr(expr),
        }
    }
}

pub struct PreparationInterpreter<'a> {
    pub world: &'a World,
    pub registers: ValueTable,
}

impl<'a> Interpreter for PreparationInterpreter<'a> {
    fn get_registers_mut(&mut self) -> &mut ValueTable {
        &mut self.registers
    }

    fn interpret_operation(&mut self, op: &Operation, value: ObjValue) -> Result<ObjValue> {
        op(self.world, value)
    }
}

impl<'a> PreparationInterpreter<'a> {
    pub fn interpret_preparation(&mut self, expression: &Expression) -> Result<Option<EventPack>> {
        match self.interpret_expr(expression) {
            Ok(value) => {
                match value {
                    ObjValue::Boolean(b) => if !b {
                            Err(action_failed(&format![
                                "preparation not met at expression\n{:?}\nWith context\n{:?}",
                                expression, self.registers
                            ]))
                    } else { Ok(None) },
                    ObjValue::EventPack(pack) => Ok(Some(pack)),
                    _ => Ok(None),
                }
            }
            Err(e) => {
                Err(e)
            }
        }
    }
}

pub fn not_found(reason: &str) -> Error {
    Error::new(ErrorKind::NotFound, reason)
}

pub fn action_failed(reason: &str) -> Error {
    Error::new(ErrorKind::Other, format!["action failed: {}", reason])
}
