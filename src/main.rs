#[macro_use]
extern crate log;
extern crate chrono;
extern crate fern;
extern crate grimoire;
extern crate grimoire_common as common;

use std::env::args;
use std::fs;
use std::io::{Error, ErrorKind};
use std::net::TcpListener;

use common::appdata;
use common::settings::Settings;
use grimoire::model::load_session::create_session_file;
use grimoire::model::load_session::{load_session, save_session};
use grimoire::model::session::Session;

fn main() -> std::io::Result<()> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Info)
        .chain(
            fs::OpenOptions::new()
                .write(true)
                .truncate(true)
                .open("log.log")?,
        )
        .apply()
        .unwrap();

    appdata::bootstrap()?;
    let mut save_name = String::new();
    let mut new_save = false;
    let mut del_save = false;

    // Skipping the executable name
    for argument in args().skip(1) {
        if argument == "--new" {
            new_save = true;
        } else if argument == "--over" {
            new_save = true;
            del_save = true;
        } else if argument == "--del" {
            del_save = true;
        } else if save_name.is_empty() {
            save_name = argument;
        }
    }

    if save_name.is_empty() {
        return Err(Error::new(
            ErrorKind::InvalidInput,
            "Please provide a save name",
        ));
    }

    let settings = Settings::default();

    if del_save {
        let mut save_path = appdata::save_dir().join(&save_name);
        save_path.set_extension("toml");

        fs::remove_file(&save_path)?;
        if !new_save {
            return Ok(());
        }
    }

    if new_save {
        info!("Creating new save: `{}`", &save_name);
        create_session_file(&save_name);
    }

    info!("Initialization complete");
    info!("Game version {}", env!("CARGO_PKG_VERSION"));
    info!("Profile version {}", settings.get_version());

    run_game(save_name)?;

    Ok(())
}

fn run_game(save_name: String) -> std::io::Result<()> {
    println!("Server running");
    let listener = TcpListener::bind("127.0.0.1:1337")?;

    let (socket, address) = listener.accept()?;
    println!("Connection established at address {}", address);

    let mut session = Session::new(save_name, socket);

    info!("Initializing session...");
    load_session(&mut session);
    session.execute();
    save_session(&session);
    info!("Session ended");
    println!("Session ended");
    Ok(())
}
