use std::collections::HashMap;
use std::ops::Index;
use std::slice;

use super::component::Component;
use common::identity::ID;

type RefMap = HashMap<ID, usize>;

pub struct Pool<T: Component> {
    data: Vec<T>,
    ids: Vec<ID>,
    id2index: RefMap,
}

impl<T: Component> Default for Pool<T> {
    fn default() -> Self {
        Pool {
            data: vec![],
            ids: vec![],
            id2index: RefMap::default(),
        }
    }
}

impl<T: Component> Pool<T> {
    pub fn at(&self, id: ID) -> Option<&T> {
        self.data.get(*self.id2index.get(&id)?)
    }

    pub fn at_mut(&mut self, id: ID) -> Option<&mut T> {
        self.data.get_mut(*self.id2index.get(&id)?)
    }

    pub fn insert(&mut self, id: ID, value: T) {
        self.data.push(value);
        self.ids.push(id);
        self.id2index.insert(id, self.data.len() - 1);
    }

    pub fn erase(&mut self, id: ID) -> bool {
        if let Some(index) = self.id2index.get(&id).map(|idx| *idx) {
            let last_id = *self.ids.last().unwrap();
            self.data.swap_remove(index);
            self.ids.swap_remove(index);
            self.id2index.insert(last_id, index);
            self.id2index.remove(&id);
            true
        } else {
            false
        }
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    pub fn ids<'a>(&'a self) -> std::slice::Iter<'a, ID> {
        self.ids.iter()
    }
}

impl<'a, T: Component> IntoIterator for &'a Pool<T> {
    type Item = &'a T;
    type IntoIter = slice::Iter<'a, T>;

    fn into_iter(self) -> slice::Iter<'a, T> {
        self.data.iter()
    }
}

impl<'a, T: Component> IntoIterator for &'a mut Pool<T> {
    type Item = &'a mut T;
    type IntoIter = slice::IterMut<'a, T>;

    fn into_iter(self) -> slice::IterMut<'a, T> {
        self.data.iter_mut()
    }
}

impl<T: Component> Index<usize> for Pool<T> {
    type Output = T;

    fn index(&self, index: usize) -> &T {
        &self.data[index]
    }
}
