
use super::components::*;
use super::save::*;
use super::session::Session;
use common::appdata;
use common::file::read_file;

use std::fs;
use std::io::prelude::*;

pub fn create_session_file(name: &str) {
    let default_save_path = appdata::db_dir().join("default_save.toml");
    let mut new_save_path = appdata::save_dir().join(name);
    new_save_path.set_extension("toml");

    if let Err(err) = fs::copy(&default_save_path, &new_save_path) {
        panic!(
            "Error while copying save file from {:?} to {:?}. Error: {:?}",
            default_save_path, new_save_path, err
        )
    }
}

pub fn load_session(session: &mut Session) {
    info!("Loading...");
    let name = session.get_profile().get_name().clone();
    let mut save_path = appdata::save_dir().join(name);
    save_path.set_extension("toml");

    match read_file(&save_path) {
        Err(err) => {
            error!(
                "Error reading save file at {:?}. Info: {:?}",
                save_path, err
            );
            panic!();
        }
        Ok(save_file_string) => match toml::from_str::<Save>(save_file_string.as_str()) {
            Err(err) => {
                error!(
                    "Error reading toml format on file {:?}. Info: {:?}",
                    save_path, err
                );
                panic!();
            },
            Ok(save) => {
                println!("{:?}", save);
                for entity_state in save.entities.into_iter() {
                    session.world.load_entity(entity_state);
                }
                session.set_time(save.session.time);
                session.world.restore_last_id(save.session.last_id);
            }
        }
    }
}

macro_rules! add_component_state {
    ($($comp_types:ty)+, $world:expr, $id:expr, $components:expr) => {
        $(
            if let Some(component) = $world.get::<$comp_types>($id) {
                $components.push(component.clone().into());
            }
        )+
    }
}

pub fn save_session(session: &Session) {
    let mut save_path = appdata::save_dir().join(session.get_profile().get_name());
    save_path.set_extension("toml");
    let mut entities = vec![];
    for id in session.world.get_ids::<Named>() {
        let mut components = vec![];
        apply_to_all_component_types![add_component_state, &session.world, id, components];
        let entity = EntityState { id, components };
        entities.push(entity);
    }
    let save = Save {
        session: SessionState {
            last_id: session.world.last_id(),
            time: session.get_time(),
        },
        entities
    };
    match fs::File::create(save_path) {
        Ok(mut file) => {
            match toml::to_string(&save) {
                Ok(serialized) => if let Err(e) = file.write_all(serialized.as_bytes()) {
                    error!("Error writing to save file! {}", e);
                    panic!()
                },
                Err(e) => {
                    error!("Error writing to save file! {}\nSave:\n{:?}", e, &save);
                    panic!()
                }
            }
        }
        Err(e) => {
            error!("Error creating save file! {}", e);
            panic!()
        }
    }
}
