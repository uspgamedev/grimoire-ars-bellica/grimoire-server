pub struct Profile {
    name: String,
}

impl Profile {
    pub fn new(name: String) -> Self {
        Profile { name }
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }
}
