extern crate anymap;
extern crate toml;

use super::component::{Component, Handle};
use super::components::Position;
use super::components::Map;
use super::components::Player;
use super::event::Event;
use super::identity::IDGenerator;
use super::pool::Pool;
use super::rulebook::dispatch_event;
use super::save::EntityState;
use super::entity_spec::EntitySpec;

use common::identity::ID;
use common::intn::Int3;

use std::collections::VecDeque;

pub struct World {
    id_generator: IDGenerator,
    pools: anymap::Map,
    event_buffers: anymap::Map,
}

struct BufferedEvent<E: Event> {
    receiver_id: ID,
    event: E,
}

type EventBuffer<E> = VecDeque<BufferedEvent<E>>;

impl World {
    pub fn init_element_pool<T: Component>(&mut self) {
        self.pools.insert::<Pool<T>>(Pool::<T>::default());
    }

    pub fn init_event_buffer<E: Event>(&mut self) {
        self.event_buffers.insert::<EventBuffer<E>>(VecDeque::default());
    }

    fn get_pool<T: Component>(&self) -> Option<&Pool<T>> {
        self.pools.get::<Pool<T>>()
    }

    fn get_pool_mut<T: Component>(&mut self) -> Option<&mut Pool<T>> {
        self.pools.get_mut::<Pool<T>>()
    }

    pub fn get_ids<T: Component>(&self) -> Vec<ID> {
        if let Some(pool) = self.get_pool::<T>() {
            pool.ids().map(|id_ref| *id_ref).collect()
        } else {
            vec![]
        }
    }

    pub fn restore_last_id(&mut self, id: ID) {
        self.id_generator.restore_last(id);
    }

    pub fn get<T: Component>(&self, id: ID) -> Option<&T> {
        match self.get_pool::<T>() {
            Some(pool) => match pool.at(id) {
                Some(component_option) => Some(component_option),
                None => None,
            },
            None => panic!("Invalid type for pool"),
        }
    }

    pub fn get_mut<T: Component>(&mut self, id: ID) -> Option<&mut T> {
        match self.get_pool_mut::<T>() {
            Some(pool) => match pool.at_mut(id) {
                Some(component_option) => Some(&mut *component_option),
                None => None,
            },
            None => panic!("Invalid type for pool"),
        }
    }

    pub fn get_handle<T: Component>(&mut self, id: ID) -> Option<Handle<T>> {
        if self.exists::<T>(id) {
            Some(Handle::<T>::new(id, self))
        } else {
            None
        }
    }

    pub fn last_id(&self) -> ID {
        self.id_generator.last()
    }

    pub fn add<T: Component>(&mut self, id: ID, component: T) {
        match self.get_pool_mut::<T>() {
            Some(pool) => pool.insert(id, component),
            None => error!("Pool for {} not found!", stringify!(T)),
        }
    }

    pub fn remove<T: Component>(&mut self, id: ID) {
        self.get_pool_mut::<T>().unwrap().erase(id);
    }

    pub fn create_at(&mut self, position: Int3, spec: EntitySpec) -> ID {
        let id = self.id_generator.next();
        self.load_entity(EntityState { id, components: spec.components });
        self.add::<Position>(id, Position::new(position));
        id
    }

    pub fn load_entity(&mut self, entity_state: EntityState) {
        let id = entity_state.id;
        for component_state in entity_state.components.into_iter() {
            component_state.add_to_entity(self, id);
        }
    }

    pub fn get_player_id(&self) -> Option<ID> {
        for id in self.get_pool::<Player>().unwrap().ids() {
            return Some(*id);
        }
        None
    }

    pub fn get_player(&self) -> Option<&Player> {
        self.get::<Player>(self.get_player_id()?)
    }

    pub fn get_player_mut(&mut self) -> Option<&mut Player> {
        self.get_mut::<Player>(self.get_player_id()?)
    }

    pub fn get_map(&self) -> &Map {
        self.get_pool::<Map>().unwrap().into_iter().next().unwrap()
    }

    pub fn get_map_id(&self) -> ID {
        *self.get_ids::<Map>().first().unwrap()
    }

    pub fn exists<T: Component>(&self, id: ID) -> bool {
        match self.get_pool::<T>() {
            Some(pool) => pool.at(id).is_some(),
            None => false,
        }
    }

    pub fn buffer_event<E: Event>(&mut self, receiver_id: ID, event: E) {
        if let Some(buffer) = self.event_buffers.get_mut::<EventBuffer<E>>() {
            buffer.push_back(BufferedEvent{ receiver_id, event });
        }
    }

    fn next_event<E: Event>(&mut self) -> Option<BufferedEvent<E>> {
        self.event_buffers.get_mut::<EventBuffer<E>>()?.pop_front()
    }

    pub fn process_events<E: Event>(&mut self) {
        while let Some(buffered_event) = self.next_event::<E>() {
            dispatch_event(self, buffered_event.receiver_id, buffered_event.event);
        }
    }
}

impl Default for World {
    fn default() -> Self {
        World {
            id_generator: IDGenerator::default(),
            pools: anymap::Map::new(),
            event_buffers: anymap::Map::new(),
        }
    }
}
