extern crate toml;

use std::collections::VecDeque;

use super::action::*;
use super::components::*;
use super::components::actor::process_actor_acts;
use super::components::damageable::check_death;
use super::components::player::write_player_journal;
use super::entity_spec::EntitySpec;
use super::event::{Effect, process_effect, all::*};
use super::profile::Profile;
use super::world::World;
use crate::script::parser::parse;
use crate::session::response::*;
use crate::session::render::render_state;
use crate::session::request::SessionRequest;

use common::identity::ID;
use common::intn::Int3;

use rand::prelude::*;
use std::io::{BufRead, BufReader, Write};
use std::net::TcpStream;

#[derive(PartialEq)]
enum Status {
    UserActionRequested,
    NoPlayer,
    REPORT,
}

pub struct Session {
    profile: Profile,
    pub world: World,
    time: u64,
    turn_order: VecDeque<ID>,
    client_request_reader: BufReader<TcpStream>,
    client_stream: TcpStream,
    next_player_effect: Option<Effect>,
    pub action_db: ActionDB,
}

macro_rules! init_element_pool {
    ($($component:ty)+, $session:expr) => {
        $($session.world.init_element_pool::<$component>();)+
    };
}

impl Session {
    pub fn new(name: String, client_stream: TcpStream) -> Self {
        let mut action_db = ActionDB::default();
        if let Err(e) = parse(&mut action_db) {
           panic!["{:?}", e];
        }
        println! {"{:?}", action_db};
        action_db.insert("nothing".into(), wrap_action(nothing));
        let mut session = Session {
            profile: Profile::new(name),
            world: World::default(),
            time: 0,
            turn_order: VecDeque::new(),
            client_request_reader: BufReader::new(client_stream.try_clone().unwrap()),
            client_stream,
            next_player_effect: None,
            action_db,
        };
        apply_to_all_component_types![init_element_pool, session];
        init_all_event_buffers(&mut session.world);
        session
    }

    pub fn get_profile(&self) -> &Profile {
        &self.profile
    }

    pub fn set_time(&mut self, time: u64) {
        self.time = time;
    }

    pub fn get_time(&self) -> u64 {
        self.time
    }

    pub fn execute(&mut self) {
        let player_id = self.world.get_player_id().unwrap().clone();
        write_player_journal(&mut self.world, player_id,
                             "Welcome to Grimoire: Ars Bellica!".to_string());
        loop {
            info!("New session loop");
            match self.run() {
                Status::NoPlayer => {
                    info!("There is no player");
                    break;
                }
                Status::REPORT => {
                    info!("Report");
                    self.send_state_if_player();
                }
                Status::UserActionRequested => {
                    info!("Awaiting user action");
                    self.send_state_if_player();
                    self.send_response(SessionResponse::ActionPending(Motive::UserTurn));
                    if !self.check_user_request() {
                        break;
                    }
                }
            }
        }
        info!("Session execution ending");
    }

    fn check_user_request(&mut self) -> bool {
        let mut buffer = String::new();
        self.client_request_reader.read_line(&mut buffer).unwrap();
        info!("request streaming in:\n{}", &buffer);
        match serde_json::from_str::<SessionRequest>(buffer.as_str()) {
            Ok(request) => match request {
                SessionRequest::EndSession => false,
                SessionRequest::TryAction(action_name, parameters) => {
                    info!("attempting action {}", &action_name);
                    match self.action_db.get(&action_name.to_string()) {
                        Some(action) => match action.compile(&self.world, parameters) {
                            Ok(effect) => {
                                self.next_player_effect = Some(effect);
                                true
                            }
                            Err(e) => {
                                // FIXME Differentiate between "user error" and "server error"
                                self.send_response(SessionResponse::ActionPending(
                                    Motive::InvalidInput(e.to_string()),
                                ));
                                self.check_user_request()
                            }
                        },
                        None => {
                            self.send_response(SessionResponse::ActionPending(
                                Motive::UnknownAction,
                            ));
                            self.check_user_request()
                        }
                    }
                }
            },
            Err(e) => {
                error!("Failed to receive session request: {}", e);
                panic!();
            }
        }
    }

    fn run(&mut self) -> Status {
        let mut loop_status: Option<Status>;
        loop {
            loop_status = None;
            if self.world.get_player_id().is_none() {
                return Status::NoPlayer;
            }
            if self.turn_order.is_empty() {
                info!("New round");
                match EntitySpec::load_from_db("monster".to_string()) {
                    Ok(spec) => {
                        let roll = thread_rng().gen_range(1, 100);
                        if roll == 1 {
                            self.world.create_at(Int3::new(10,10,2), spec);
                        }
                    },
                    Err(err) => {
                        println!("failed to spawn monster: {}", err);
                    }
                }
                for actor_id in self.world.get_ids::<Actor>() {
                    self.turn_order.push_back(actor_id);
                }
            }
            while let Some(next_id) = self.turn_order.front().map(|id| *id) {
                let mut action_happened = false;
                if self.world.exists::<Actor>(next_id) {
                    if self.world.get::<Actor>(next_id).unwrap().is_ready() {
                        info!("Actor #{}'s turn", next_id);
                        let effect = if self.world.exists::<Player>(next_id) {
                            if self.next_player_effect.is_some() {
                                self.next_player_effect.take().unwrap()
                            } else {
                                return Status::UserActionRequested;
                            }
                        } else {
                            let mut agent = self.world.get_handle::<Agent>(next_id).unwrap();
                            agent.decide_action(&self.action_db)
                        };
                        process_effect(&mut self.world, effect);
                        action_happened = true;
                    }
                    if action_happened {
                        info!("actor {}'s action succeeded", next_id);
                        loop_status = Some(Status::REPORT);
                    }
                }
                self.turn_order.pop_front();
            }
            for actor_id in self.world.get_ids::<Actor>() {
                self.world.get_mut::<Actor>(actor_id).unwrap().tick();
                process_actor_acts(&mut self.world, actor_id.clone());
            }
            process_all_events(&mut self.world);
            for damageable_id in self.world.get_ids::<Damageable>() {
                check_death(&mut self.world, damageable_id.clone());
            }
            info!("End of round");
            self.time += 1;
            if loop_status.is_some() {
                return loop_status.unwrap();
            }
        }
    }

    pub fn send_state_if_player(&mut self) {
        if self.world.get_player().is_some() {
            let state = render_state(self);
            info!(
                "state player position: {}",
                state.entities_info[&state.player_state.id].maybe_position.as_ref().unwrap()
            );
            self.send_response(SessionResponse::StateChanged(state));
        }
    }

    fn send_response(&mut self, response: SessionResponse) {
        let buffer = serde_json::to_string(&response).unwrap() + &("\n".to_string());
        debug!("streaming out:\n{}", &buffer);
        self.client_stream.write_all(buffer.as_bytes()).unwrap();
    }
}
