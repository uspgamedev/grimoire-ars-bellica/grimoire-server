
use super::save::ComponentState;
use common::appdata;
use common::file::read_file;

use std::io::Error;

#[derive(Deserialize)]
pub struct EntitySpec {
    pub components: Vec<ComponentState>
}

impl EntitySpec {
    pub fn load_from_db(key: String) -> Result<Self, Error> {
        let specs_path = appdata::db_dir().join(format!["entities/{}.toml", key]);
        let specs_string = read_file(&specs_path)?;
        Ok(toml::from_str(specs_string.as_str())?)
    }
}

