extern crate toml;

use super::world::World;
use common::identity::ID;

use serde::de::DeserializeOwned;
use serde::Serialize;

use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

pub struct Handle<'a, T: Component> {
    pub id: ID,
    pub world: &'a mut World,
    component_type: PhantomData<T>,
}

impl<'a, T: Component> Handle<'a, T> {
    pub fn new(id: ID, world: &'a mut World) -> Self {
        Handle::<T> {
            id,
            world,
            component_type: PhantomData,
        }
    }

    pub fn as_component<X: Component>(&'a self) -> Option<&'a X> {
        self.world.get::<X>(self.id)
    }

    pub fn select<X: Component>(&'a mut self) -> Option<Handle<'a, X>> {
        self.world.get_handle::<X>(self.id)
    }

    pub fn has<X: Component>(&self) -> bool {
        self.world.exists::<X>(self.id)
    }
}

impl<'a, T: Component> Deref for Handle<'a, T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.world.get::<T>(self.id).unwrap()
    }
}

impl<'a, T: Component> DerefMut for Handle<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        self.world.get_mut::<T>(self.id).unwrap()
    }
}

pub trait IDable {
    fn get_id(&self) -> ID;
}

pub trait Component: Clone + Sized + 'static + Serialize + DeserializeOwned {
    fn add_to_entity(self, world: &mut World, id: ID) {
        world.add::<Self>(id, self);
    }
}
