use common::identity::ID;

#[derive(Default)]
pub struct IDGenerator {
    next_id: ID,
}

impl IDGenerator {
    pub fn restore_last(&mut self, id: ID) {
        self.next_id = id + 1
    }

    pub fn last(&self) -> ID {
        self.next_id - 1
    }

    pub fn next(&mut self) -> ID {
        let new_id = self.next_id;
        self.next_id += 1;
        new_id
    }
}
