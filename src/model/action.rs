
use super::world::World;
use super::event::Effect;
use crate::core::ValueTable;

use std::collections::HashMap;
use std::io::Result;

pub trait Action: std::fmt::Debug {
    fn compile(&self, world: &World, parameters: ValueTable) -> Result<Effect>;
}

pub type ActionDB = HashMap<String, Box<dyn Action>>;

pub struct FnAction {
    function: fn(&World, ValueTable) -> Result<Effect>
}

impl Action for FnAction {
    fn compile(&self, world: &World, parameters: ValueTable) -> Result<Effect> {
        (self.function)(world, parameters)
    }
}

impl std::fmt::Debug for FnAction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("<native function>")
    }
}

pub fn wrap_action(function: fn(&World, ValueTable) -> Result<Effect>) -> Box<dyn Action> {
    Box::new(FnAction{ function })
}

pub fn nothing(_world: &World, _parameters: ValueTable) -> Result<Effect> {
    Ok(Effect::default())
}

//pub fn place(world: &World, parameters: ValueTable) -> Result<Effect> {
//    let (self_entity, position, with_entity) = {
//        if let (Some(Value::Entity(self_entity)),
//                Some(Value::Int3(position)),
//                Some(Value::Entity(with_entity))) = (parameters.get("Self".into()),
//                                                     parameters.get("Pos".into()),
//                                                     parameters.get("With".into())) {
//            Ok((self_entity, position, with_entity))
//        } else {
//            Err(invalid_value(""))
//        }
//    }?;
//    //if let Some(self_manipulator) {}
//
//    Ok(Effect::default())
//}

