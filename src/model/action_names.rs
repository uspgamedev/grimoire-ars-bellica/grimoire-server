use common::aliases::*;

pub const PICKUP: StringLiteral = "pick_up";
pub const OPEN: StringLiteral = "open";
pub const CLOSE: StringLiteral = "close";
pub const UNARMED_STRIKE_AT: StringLiteral = "unarmed_strike";
pub const WEAPON_STRIKE_AT: StringLiteral = "weapon_strike";
pub const MOVE: StringLiteral = "move";
pub const IDLE: StringLiteral = "idle";
