use super::EventPack;
use crate::model::world::World;

pub type Effect = Vec<EventPack>;

pub fn process_effect(world: &mut World, effect: Effect) {
    for pack in effect.into_iter() {
        pack.send(world);
    }
}
