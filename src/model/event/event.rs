use crate::model::world::World;
use common::identity::ID;
use enum_dispatch::enum_dispatch;

pub trait Event: 'static {
    fn dummy(&self) {}
}

#[enum_dispatch]
pub trait EventSender: Event {
    fn send(self, world: &mut World, receiver_id: ID);
}

pub trait Resolution: Event {
    fn resolve(self, world: &mut World, source: ID) -> Option<()>;
}

impl<E: Event> EventSender for E {
    default fn send(self, world: &mut World, receiver_id: ID) {
        world.buffer_event(receiver_id, self);
    }
}
