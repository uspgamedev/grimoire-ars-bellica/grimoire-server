use super::all::AnyEvent;
use super::event::EventSender;
use crate::model::world::World;
use common::identity::ID;

#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
pub struct EventPack {
    receiver_id: ID,
    event: AnyEvent,
}

impl EventPack {
    pub fn new(receiver_id: ID, event: AnyEvent) -> Self {
        EventPack { receiver_id, event }
    }
    pub fn send(self, world: &mut World) {
        self.event.send(world, self.receiver_id);
    }
}
