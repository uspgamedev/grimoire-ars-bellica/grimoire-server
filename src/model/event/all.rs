use super::Event;
use super::event::EventSender;
use crate::model::components::actor::Act;
use crate::model::world::World;
use common::directions::Direction;
use common::identity::ID;
use common::intn::Int3;
use enum_dispatch::enum_dispatch;

macro_rules! define_event {
    (struct $name:ident $block:tt) => {
        #[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
        pub struct $name $block
        impl Event for $name {}
    };
    (enum $name:ident $block:tt) => {
        #[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
        pub enum $name $block
        impl Event for $name {}
    }
}

macro_rules! define_event_enum {
    ($($name:ident)+) => {
        #[enum_dispatch(EventSender)]
        #[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
        pub enum AnyEvent {
            $($name($name),)+
        }
        impl Event for AnyEvent {}
    }
}

macro_rules! define_event_init {
    ($($names:ident)+) => {
        pub fn init_all_event_buffers(world: &mut World) {
            $(world.init_event_buffer::<$names>();)*
        }
    }
}

macro_rules! define_event_process {
    ($($names:ident)+) => {
        pub fn process_all_events(world: &mut World) {
            $(world.process_events::<$names>();)*
        }
    }
}

macro_rules! define_all_events {
    ($([NAME] $names:ident)*) => {
        define_event_enum![$($names)*];
        define_event_init![$($names)*];
        define_event_process![$($names)*];
    };
    (struct $name:ident $block:tt $($kw:ident $next_names:ident $next_blocks:tt)* $([NAME] $names:ident)* ) => {
        define_event![struct $name $block];
        
        define_all_events!($($kw $next_names $next_blocks)* $([NAME] $names)* [NAME] $name);
    };
    (enum $name:ident $block:tt $($kw:ident $next_names:ident $next_blocks:tt)* $([NAME] $names:ident)* ) => {
        define_event![enum $name $block];
        
        define_all_events!($($kw $next_names $next_blocks)* $([NAME] $names)* [NAME] $name);
    };
}

define_all_events!{

    struct ActEvent {
        pub acts: Vec<Act>,
    }

    struct RestEvent;

    enum MoveEvent {
        Direction(Direction),
        Displacement(Int3),
    }

    struct StoreEvent {
        pub stored_entity: ID
    }

    struct PlaceEvent {
        pub position: Int3,
    }

    struct DisownEvent {
        pub disowned_entity: ID
    }

    struct DirectStrikeEvent {
        pub target_entity: ID,
        pub weapon_entity: ID,
    }

    struct TakeDamageEvent {
        pub amount: usize,
    }

    struct DieEvent;

    struct OpenEvent;

    struct CloseEvent;

    struct ObserveAppearanceChangeEvent {
        pub observed_id: ID,
        pub to_what: String,
    }

    struct ObserveBehaviorEvent {
        pub observed_id: ID,
        pub behavior: String,
    }

}

//impl Resolution for RestEvent {
//    fn resolve(self, world: &mut World, source: ID) -> Option<()> {
//        write_player_journal(world, source, "You take a rest".into());
//    }
//}

