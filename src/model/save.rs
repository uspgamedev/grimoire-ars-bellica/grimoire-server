
use super::world::World;
use super::components::*;
use super::component::Component;
use common::identity::ID;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Save {
    pub session: SessionState,
    pub entities: Vec<EntityState>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SessionState {
    pub last_id: ID,
    pub time: u64
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EntityState {
    pub id: ID,
    pub components: Vec<ComponentState>
}

macro_rules! define_component_state {
    ($($comp_types:ident)+, $_unused:expr) => {
        #[serde(tag = "type")]
        #[derive(Clone, Debug, Serialize, Deserialize)]
        pub enum ComponentState {
            $($comp_types($comp_types),)+
        }
        impl Component for ComponentState {
            fn add_to_entity(self, world: &mut World, id: ID) {
                match self {
                    $(
                        ComponentState::$comp_types(component) => {
                            component.add_to_entity(world, id)
                        },
                    )+
                }
            }
        }
        $(
            impl From<$comp_types> for ComponentState {
                fn from(component: $comp_types) -> Self {
                    ComponentState::$comp_types(component)
                }
            }
        )+
    }
}

apply_to_all_component_types![define_component_state, true];

