use super::*;

use super::owned::get_owner;
use common::directions::*;
use common::intn::{IntN, Int3};

pub const WALK_COST: usize = 50;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Position {
    #[serde(flatten)]
    coords: Int3,
}

impl Position {
    pub fn new(coords: Int3) -> Self {
        Self { coords }
    }

    pub fn squared_distance_to(&self, other_coords: &Int3) -> usize {
        let diff = other_coords - &self.coords;
        diff.dot(&diff) as usize
    }

    pub fn is_within(&self, other_coords: &Int3, distance: usize) -> bool {
        self.squared_distance_to(other_coords) <= distance * distance
    }

    pub fn valid_move_directions(&self, world: &World) -> Vec<Direction> {
        let mut valid_dirs = vec![];
        for idx in 0..DIRECTIONS.len() {
            let dir = Int3::from(&DIRECTIONS[idx]);
            let pos = self.coords.clone() + dir;
            if world.get_map().is_tile_valid(world, &pos) {
                valid_dirs.push(Direction::from(idx));
            }
        }
        valid_dirs
    }
}

impl<'a> Handle<'a, Position> {
    pub fn move_along(&mut self, movement: &Int3) -> bool {
        let next_coords = get_entity_position(self.world, self.id).unwrap() + movement;
        let map = self.world.get_map();
        if map.is_tile_inside(&next_coords) && map.is_tile_passable(self.world, &next_coords) {
            self.coords = self.coords.clone() + movement;
            return true;
        }
        false
    }
}

impl Rule<MoveEvent> for Rulebook<Position> {
    fn apply(&self, world: &mut World, event: &mut MoveEvent) -> Option<Resolution> {
        let movement = match event {
            MoveEvent::Direction(dir) => Int3::from(dir.clone()),
            MoveEvent::Displacement(vec) => vec.clone(),
        };
        if world.get_handle::<Position>(self.id)?.move_along(&movement) {
            Some(Resolution::Partial)
        } else {
            None
        }
    }
}

pub fn get_entity_position(world: &World, entity: ID) -> Option<Int3> {
    if let Some(position) = world.get::<Position>(entity).map(|p| p.coords.clone()) {
        Some(position)
    } else {
        let owner = get_owner(world, entity);
        if owner != entity {
            get_entity_position(world, owner)
        } else {
            None
        }
    }
}

pub fn get_strict_entity_position(world: &World, entity: ID) -> Option<Int3> {
    world.get::<Position>(entity).map(|p| p.coords.clone())
}

pub fn get_entity_ids_at(world: &World, pos: &Int3) -> Vec<ID> {
    let mut list = Vec::new();
    for position_id in world.get_ids::<Position>() {
        if let Some(true) = get_entity_position(world, position_id).map(|p| p == *pos) {
            list.push(position_id)
        }
    }
    list
}

pub fn is_within_distance_of_entity(world: &World, pos: Int3, distance: f64, entity: ID) -> bool {
    if let Some(entity_pos) = get_entity_position(world, entity) {
        entity_pos.is_within_distance_of(&pos, distance)
    } else {
        false
    }
}

pub fn find_all_within_distance(world: &World, pos: Int3, distance: f64) -> Vec<ID> {
    let mut entities = vec![];
    for entity in world.get_ids::<Named>() {
        if is_within_distance_of_entity(world, pos, distance, entity) {
            entities.push(entity);
        }
    }
    entities
}
