use super::*;
use super::named::get_name;
use super::seer::find_all_observers;
use common::appearance::Appearance;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Visible {
    appearance: Appearance,
}

impl Visible {
    pub fn new(appearance: Appearance) -> Self {
        Self { appearance }
    }

    pub fn get_appearance(&self) -> Appearance {
        self.appearance
    }

    pub fn set_appearance(&mut self, new_appearance: Appearance) {
        self.appearance = new_appearance;
    }
}

impl Rule<OpenEvent> for Rulebook<Visible> {
    fn apply(&self, world: &mut World, _event: &mut OpenEvent) -> Option<Resolution> {
        let appearance = world.get::<Openable>(self.id)?.opened_appearance;
        let mut visible = world.get_mut::<Visible>(self.id)?;
        visible.appearance = appearance;
        for observer_id in find_all_observers(world, self.id) {
            let observe_event = ObserveAppearanceChangeEvent{
                observed_id: self.id,
                to_what: "open".to_string(),
            };
            world.buffer_event(observer_id, observe_event);
        }
        Some(Resolution::Partial)
    }
}

impl Rule<CloseEvent> for Rulebook<Visible> {
    fn apply(&self, world: &mut World, _event: &mut CloseEvent) -> Option<Resolution> {
        let appearance = world.get::<Openable>(self.id)?.closed_appearance;
        let mut visible = world.get_mut::<Visible>(self.id)?;
        visible.appearance = appearance;
        for observer_id in find_all_observers(world, self.id) {
            let observe_event = ObserveAppearanceChangeEvent{
                observed_id: self.id,
                to_what: "closed".to_string(),
            };
            world.buffer_event(observer_id, observe_event);
        }
        Some(Resolution::Partial)
    }
}

impl Rule<DirectStrikeEvent> for Rulebook<Visible> {
    fn apply(&self, world: &mut World, event: &mut DirectStrikeEvent) -> Option<Resolution> {
        for observer_id in find_all_observers(world, self.id) {
            let observe_event = ObserveBehaviorEvent{
                observed_id: self.id,
                behavior: format!["struck {:?}", get_name(world, event.target_entity)],
            };
            world.buffer_event(observer_id, observe_event);
        }
        Some(Resolution::Partial)
    }
}

impl Rule<DieEvent> for Rulebook<Visible> {
    fn apply(&self, world: &mut World, _event: &mut DieEvent) -> Option<Resolution> {
        for observer_id in find_all_observers(world, self.id) {
            let observe_event = ObserveBehaviorEvent{
                observed_id: self.id,
                behavior: "dies".to_string(),
            };
            world.buffer_event(observer_id, observe_event);
        }
        Some(Resolution::Partial)
    }
}
