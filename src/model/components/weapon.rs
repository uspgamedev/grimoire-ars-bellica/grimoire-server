use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Weapon {
    pub power: usize,
}

pub fn get_weapon_power(world: &World, maybe_id: Option<ID>) -> Option<usize> {
    Some(world.get::<Weapon>(maybe_id?)?.power)
}
