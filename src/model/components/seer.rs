use super::*;

use super::position::{get_entity_position, is_within_distance_of_entity};
use common::intn::{IntN, Int3};

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Seer {
    range: usize,
}

impl Seer {
    pub fn new(range: usize) -> Self {
        Self { range }
    }

    pub fn get_range(&self) -> usize {
        self.range
    }
}

pub fn get_all_observed_by(world: &World, observer_id: ID) -> Vec<ID> {
    let mut observed = vec![];
    if let (Some(seer), Some(observer_position)) = (world.get::<Seer>(observer_id),
                                                    get_entity_position(world, observer_id)) {
        for position_id in world.get_ids::<Position>() {
            let observed_position = get_entity_position(world, position_id).unwrap();
            let dist = observer_position.distance_to(&observed_position);
            if dist <= (seer.range as f64) && world.exists::<Visible>(position_id) {
                observed.push(position_id);
            }
        }
    }
    observed
}

pub fn list_all_visible_positions(world: &World, observer: ID) -> Vec<Int3> {
    let mut positions = vec![];
    if let (Some(seer), Some(observer_position)) = (world.get::<Seer>(observer),
                                                    get_entity_position(world, observer)) {
        let range = seer.range as i64;
        for x in (observer_position.x - range)..(observer_position.x + range + 1) {
            for y in (observer_position.y - range)..(observer_position.y + range + 1) {
                let position = Int3::new(x, y, observer_position.z);
                if position.distance_to(&observer_position) <= (range as f64) {
                    positions.push(position);
                }
            }
        }
    }
    positions
}

pub fn find_all_observers(world: &World, observed_id: ID) -> Vec<ID> {
    let mut observers = vec![];
    if let (Some(observed_position), true) = (get_entity_position(world, observed_id),
                                              world.exists::<Visible>(observed_id)) {
        for seer_id in world.get_ids::<Seer>() {
            let range = world.get::<Seer>(seer_id).unwrap().range as f64;
            if is_within_distance_of_entity(world, observed_position.clone(), range, seer_id) {
                observers.push(seer_id);
            }
        }
    }
    observers
}
