use super::*;
use crate::model::event::EventPack;
use std::cmp::min;
use std::collections::VecDeque;

pub const MAX_ENERGY: usize = 1000;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Actor {
    energy: usize,
    speed: usize,
    act_queue: VecDeque<Act>
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Act {
    pub cost: usize,
    pub pack: EventPack,
}

impl Actor {
    pub fn new(speed: usize) -> Self {
        Actor { energy: 0, speed, act_queue: VecDeque::default() }
    }
    pub fn is_ready(&self) -> bool {
        self.energy >= MAX_ENERGY && self.act_queue.is_empty()
    }

    pub fn spend_energy(&mut self, amount: usize) {
        self.energy -= min(amount, self.energy);
    }

    pub fn tick(&mut self) {
        self.energy = self.energy + self.speed;
    }
}

impl Act {
    pub fn new(cost: usize, pack: EventPack) -> Self {
        Act { cost, pack }
    }
}

impl Rule<ActEvent> for Rulebook<Actor> {
    fn apply(&self, world: &mut World, event: &mut ActEvent) -> Option<Resolution> {
        for act in event.acts.iter() {
            queue_actor_act(world, self.id, act.clone())
        }
        Some(Resolution::Partial)
    }
}

impl Rule<DieEvent> for Rulebook<Actor> {
    fn apply(&self, world: &mut World, _event: &mut DieEvent) -> Option<Resolution> {
        world.remove::<Actor>(self.id);
        Some(Resolution::Partial)
    }
}

pub fn spend_actor_energy(world: &mut World, id: ID, amount: usize) {
    if let Some(actor) = world.get_mut::<Actor>(id) {
        actor.spend_energy(amount);
    }
}

pub fn can_actor_spend(world: &mut World, id: ID, amount: usize) -> bool {
    if let Some(actor) = world.get::<Actor>(id) {
        amount <= actor.energy
    } else {
        false
    }
}

pub fn queue_actor_act(world: &mut World, id: ID, act: Act) {
    if let Some(actor) = world.get_mut::<Actor>(id) {
        actor.act_queue.push_back(act);
    }
}

pub fn pop_actor_act(world: &mut World, id: ID) -> Option<Act> {
    world.get_mut::<Actor>(id)?.act_queue.pop_front()
}

pub fn rewind_actor_act(world: &mut World, id: ID, act: Act) {
    if let Some(actor) = world.get_mut::<Actor>(id) {
        actor.act_queue.push_front(act);
    }
}

pub fn process_actor_acts(world: &mut World, id: ID) {
    while let Some(act) = pop_actor_act(world, id) {
        if can_actor_spend(world, id, act.cost) {
            spend_actor_energy(world, id, act.cost);
            act.pack.send(world);
        } else {
            rewind_actor_act(world, id, act);
            break;
        }
    }
}
