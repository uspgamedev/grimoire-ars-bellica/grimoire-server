extern crate grimoire_macros;

use grimoire_macros::Component;

use super::component::{Component, Handle};
use super::event::all::*;
use super::rulebook::*;
use super::world::World;
use common::identity::ID;

pub mod actor;
pub mod agent;
pub mod applicable;
pub mod attacker;
pub mod carryable;
pub mod container;
pub mod damageable;
pub mod interactive;
pub mod manipulator;
pub mod map;
pub mod named;
pub mod obstacle;
pub mod openable;
pub mod owned;
pub mod player;
pub mod position;
pub mod seer;
pub mod visible;
pub mod weapon;

pub use actor::Actor;
pub use agent::Agent;
pub use applicable::Applicable;
pub use attacker::Attacker;
pub use carryable::Carryable;
pub use container::Container;
pub use damageable::Damageable;
pub use interactive::Interactive;
pub use manipulator::Manipulator;
pub use map::Map;
pub use named::Named;
pub use obstacle::Obstacle;
pub use openable::Openable;
pub use owned::Owned;
pub use player::Player;
pub use position::Position;
pub use seer::Seer;
pub use visible::Visible;
pub use weapon::Weapon;

#[macro_export]
macro_rules! apply_to_all_component_types {
    ($operation:ident, $($args:expr),+) => {
        $operation![
            Visible
            Actor
            Agent
            Applicable
            Attacker
            Carryable
            Container
            Damageable
            Interactive
            Manipulator
            Map
            Named
            Obstacle
            Openable
            Owned
            Player
            Position
            Seer
            Weapon,
            $($args),+
        ];
    }
}
