use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Player {
    pub journal: Vec<String>
}

impl Rule<ActEvent> for Rulebook<Player> {
    fn apply(&self, world: &mut World, event: &mut ActEvent) -> Option<Resolution> {
        let mut cost = 0;
        for act in event.acts.iter() {
            cost += act.cost;
        }
        write_player_journal(world, self.id, format!["You make an action worth {} energy.", cost]);
        Some(Resolution::Partial)
    }
}

impl Rule<RestEvent> for Rulebook<Player> {
    fn apply(&self, world: &mut World, _event: &mut RestEvent) -> Option<Resolution> {
        write_player_journal(world, self.id, "You take a rest".to_string());
        Some(Resolution::Partial)
    }
}

impl Rule<ObserveAppearanceChangeEvent> for Rulebook<Player> {
    fn apply(&self, world: &mut World, event: &mut ObserveAppearanceChangeEvent) -> Option<Resolution> {
        if world.exists::<Seer>(self.id) {
            let name = world.get::<Named>(event.observed_id)?.get_name().clone();
            write_player_journal(world, self.id, format!["The {} is {}", name, event.to_what]);
            Some(Resolution::Partial)
        } else {
            None
        }
    }
}

impl Rule<ObserveBehaviorEvent> for Rulebook<Player> {
    fn apply(&self, world: &mut World, event: &mut ObserveBehaviorEvent) -> Option<Resolution> {
        if world.exists::<Seer>(self.id) {
            let name = world.get::<Named>(event.observed_id)?.get_name().clone();
            write_player_journal(world, self.id, format!["The {} {}", name, event.behavior]);
            Some(Resolution::Partial)
        } else {
            None
        }
    }
}

impl Rule<DieEvent> for Rulebook<Player> {
    fn apply(&self, world: &mut World, _event: &mut DieEvent) -> Option<Resolution> {
        world.remove::<Player>(self.id);
        Some(Resolution::Partial)
    }
}

pub fn write_player_journal(world: &mut World, id: ID, log: String) {
    if let Some(player) = world.get_mut::<Player>(id) {
        player.journal.push(log);
    }
}

