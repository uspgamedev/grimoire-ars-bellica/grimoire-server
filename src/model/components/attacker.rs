use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Attacker {
    skill: usize,
    power: usize,
}

impl Attacker {
    pub fn new(skill: usize, power: usize) -> Self {
        Self { skill, power }
    }
    pub fn get_skill(&self) -> usize {
        self.skill
    }

    pub fn get_power(&self) -> usize {
        self.power
    }
}
