use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Carryable {
    weight: usize,
}

pub fn is_carried_by(world: &World, carried: ID, carrier: ID) -> bool {
    if world.exists::<Carryable>(carried) {
        if let Some(manipulator) = world.get::<Manipulator>(carrier) {
            if manipulator.carried_entity.map(|e| e == carried) == Some(true) {
                return true;
            }
        } else if let Some(container) = world.get::<Container>(carrier) {
            if container.has_item(carried) {
                return true;
            }
        }
        // TODO recursive case
        // TODO owned containers and manipulators
    }
    false
}

//impl Rule<PlaceEvent> for Rulebook<Carryable> {
//    fn resolve(&self, world: &mut World, event: &mut PlaceEvent) -> Option<Resolution> {
//        // WIP this is all wrong
//        let owner_entity = get_owner(world, self.id);
//        if owner_entity != self.id {
//            let disown_event = DisownEvent { disowned_entity: self.id };
//            world.buffer_event(owner_entity, disown_event);
//            world.remove::<Owned>(self.id);
//        }
//        if let Some(position) = world.get_mut::<Position>(self.id) {
//            position.coords = event.position.clone();
//        } else {
//            world.add(self.id, Position{ coords: event.position.clone() });
//        }
//        Some(Resolution::Partial)
//    }
//}
