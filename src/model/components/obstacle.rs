use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Obstacle;

impl Rule<DieEvent> for Rulebook<Obstacle> {
    fn apply(&self, world: &mut World, _event: &mut DieEvent) -> Option<Resolution> {
        world.remove::<Obstacle>(self.id);
        Some(Resolution::Partial)
    }
}

