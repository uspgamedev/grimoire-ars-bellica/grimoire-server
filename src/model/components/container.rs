use super::*;

use super::position::Position;
use common::identity::IDList;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Container {
    contents: IDList,
}

impl Container {
    pub fn has_item(&self, id: ID) -> bool {
        for item_id in &self.contents {
            if *item_id == id {
                return true;
            }
        }
        false
    }

    pub fn get_held_entities(&self) -> IDList {
        self.contents.clone()
    }
}

impl Rule<StoreEvent> for Rulebook<Container> {
    fn apply(&self, world: &mut World, event: &mut StoreEvent) -> Option<Resolution> {
        if world.get::<Container>(self.id)?.has_item(event.stored_entity)
        || !world.exists::<Carryable>(event.stored_entity) {
            return None;
        }
        if world.exists::<Position>(event.stored_entity) {
            world.remove::<Position>(event.stored_entity);
        }
        if world.exists::<Owned>(event.stored_entity) {
            world.get_mut::<Owned>(event.stored_entity)?.owner = self.id
        } else {
            world.add(event.stored_entity, Owned { owner: self.id })
        }
        world.get_mut::<Container>(self.id)?.contents.push(event.stored_entity);
        Some(Resolution::Partial)
    }
}
