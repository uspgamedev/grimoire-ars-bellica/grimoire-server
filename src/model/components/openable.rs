use super::*;

use common::appearance::Appearance;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Openable {
    pub open: bool,
    pub opened_appearance: Appearance,
    pub closed_appearance: Appearance,
}

impl Openable {
    pub fn is_open(&self) -> bool {
        self.open
    }

    pub fn toggle_open(&mut self) {
        self.open = !self.open;
    }

    pub fn get_appearance(&self) -> Appearance {
        if self.open {
            self.opened_appearance
        } else {
            self.closed_appearance
        }
    }
}

impl Rule<OpenEvent> for Rulebook<Openable> {
    fn apply(&self, world: &mut World, _event: &mut OpenEvent) -> Option<Resolution> {
        let mut openable = world.get_mut::<Openable>(self.id)?;
        openable.open = true;
        world.remove::<Obstacle>(self.id);
        Some(Resolution::Partial)
    }
}

impl Rule<CloseEvent> for Rulebook<Openable> {
    fn apply(&self, world: &mut World, _event: &mut CloseEvent) -> Option<Resolution> {
        let mut openable = world.get_mut::<Openable>(self.id)?;
        openable.open = false;
        world.add(self.id, Obstacle{});
        Some(Resolution::Partial)
    }
}

impl Rule<DieEvent> for Rulebook<Openable> {
    fn apply(&self, world: &mut World, _event: &mut DieEvent) -> Option<Resolution> {
        world.remove::<Openable>(self.id);
        Some(Resolution::Partial)
    }
}
