use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Interactive {
    pub interactions: Vec<String>,
}
