use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Owned {
    pub owner: ID,
}

impl Owned {
    pub fn new(owner: ID) -> Self {
        Self { owner }
    }
}

pub fn get_owner(world: &World, owned_entity: ID) -> ID {
    if let Some(owned) = world.get::<Owned>(owned_entity) {
        owned.owner
    } else {
        owned_entity
    }
}

pub fn get_all_owned_by(world: &World, owner_entity: ID) -> Vec<ID> {
    let mut owned_entities = vec![];
    for owned_entity in world.get_ids::<Owned>() {
        if owner_entity == get_owner(world, owned_entity) {
            owned_entities.push(owned_entity);
        }
    }
    owned_entities
}

