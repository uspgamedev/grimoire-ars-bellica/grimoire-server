use super::*;
use super::owned::get_all_owned_by;
use super::position::{get_entity_position, find_all_within_distance};

// Question: do we filter possible positions and targets somehow?
#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Applicable {
    pub to_positions: Vec<String>,
    pub to_targets: Vec<String>,
}

pub fn find_all_applicable_by(world: &World, by_entity: ID) -> Vec<ID> {
    let mut all_applicable = get_all_owned_by(world, by_entity);
    if let (Some(position), Some(manipulator)) = (get_entity_position(world, by_entity),
                                                  world.get::<Manipulator>(by_entity)) {
        let mut nearby = find_all_within_distance(world, position, manipulator.get_reach() as f64);
        all_applicable.append(&mut nearby);
    }
    all_applicable.sort();
    all_applicable.dedup();
    all_applicable
}
