use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Named {
    name: String,
}

impl Named {
    pub fn new(name: String) -> Self {
        Self { name }
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }
}

pub fn get_name(world: &World, entity: ID) -> String {
    if let Some(name) = world.get::<Named>(entity).map(|e| e.name.clone()) {
        format!["{}#{:?}", name, entity]
    } else {
        String::from("unknown")
    }
}
