use super::*;

use super::super::super::common::intn::Int3;
use super::position::get_entity_ids_at;
use common::tilemap::{TileID, TileMap};

pub const BEDROCK_LEVEL: usize = 0;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Map {
    tilemap: TileMap,
}

impl Map {
    pub fn is_tile_inside(&self, pos: &Int3) -> bool {
        self.tilemap.is_tile_inside(pos)
    }

    pub fn get_tile(&self, pos: &Int3) -> TileID {
        self.tilemap.get_tile(pos)
    }

    pub fn get_width(&self) -> usize {
        self.tilemap.get_width()
    }

    pub fn get_height(&self) -> usize {
        self.tilemap.get_height()
    }

    pub fn get_depth(&self) -> usize {
        self.tilemap.get_depth()
    }

    pub fn clone_tilemap(&self) -> TileMap {
        self.tilemap.clone()
    }

    pub fn is_tile_passable(&self, world: &World, pos: &Int3) -> bool {
        if self.get_tile(pos) == TileID::EMPTY {
            for id in get_entity_ids_at(world, pos) {
                if world.exists::<Obstacle>(id) {
                    return false;
                }
            }
            true
        } else {
            false
        }
    }

    pub fn is_tile_valid(&self, world: &World, pos: &Int3) -> bool {
        self.is_tile_inside(pos) && self.is_tile_passable(world, pos)
    }
}
