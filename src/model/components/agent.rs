extern crate rand;

use super::*;
use super::position::get_entity_position;
use super::seer::get_all_observed_by;
use crate::core::{Value, ValueTable};
use crate::model::action::ActionDB;
use crate::model::event::Effect;
use crate::util::geometry::closest_direction;

use common::directions::*;
use common::intn::IntN;

use rand::prelude::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Agent;

impl Agent {
    pub fn new() -> Self {
        Agent{}
    }
}

impl<'a> Handle<'a, Agent> {
    pub fn decide_action(&mut self, action_db: &ActionDB) -> Effect {
        let valid_dirs = self
            .as_component::<Position>()
            .map(|position| position.valid_move_directions(self.world))
            .unwrap_or(vec![]);
        if let Some(effect) = self.go_after_player(&action_db, &valid_dirs) {
            return effect;
        } else if valid_dirs.len() > 0 {
            let idx = thread_rng().gen_range(0, valid_dirs.len()) as usize;
            let params = table![
                "Self" => self.id,
                "Dir" => valid_dirs[idx].clone()
            ];
            if let Some(effect) = self.compile_action(action_db, "walk", params) {
                return effect;
            }
        }
        return self
            .compile_action(action_db, "idle", table!["Self" => self.id])
            .unwrap();
    }

    fn go_after_player(
        &mut self,
        action_db: &ActionDB,
        valid_dirs: &Vec<Direction>,
    ) -> Option<Effect> {
        let this_position = get_entity_position(self.world, self.id)?;
        let player_id = spot_player_id(self.world, self.id)?;
        let player_position = get_entity_position(self.world, player_id)?;
        let diff = &player_position - this_position;
        let manipulator = self.as_component::<Manipulator>()?;
        if self.has::<Weapon>() && diff.size() <= manipulator.get_reach() {
            let params = table![
                "Self" => self.id,
                "Target" => player_id,
                "With" => self.id
            ];
            self.compile_action(action_db, "weapon_strike", params)
        } else if valid_dirs.len() > 0 {
            let dir = closest_direction(&valid_dirs, &diff);
            let params = table!["Self" => self.id, "Dir" => dir];
            self.compile_action(action_db, "walk", params)
        } else {
            None
        }
    }

    fn compile_action(
        &mut self,
        action_db: &ActionDB,
        name: &'static str,
        parameters: ValueTable,
    ) -> Option<Effect> {
        action_db.get(name)?.compile(&self.world, parameters).ok()
    }
}

impl Rule<DieEvent> for Rulebook<Agent> {
    fn apply(&self, world: &mut World, _event: &mut DieEvent) -> Option<Resolution> {
        world.remove::<Agent>(self.id);
        Some(Resolution::Partial)
    }
}


fn spot_player_id(world: &World, observer_id: ID) -> Option<ID> {
    for other_entity_id in get_all_observed_by(world, observer_id) {
        if world.exists::<Player>(other_entity_id) {
            return Some(other_entity_id);
        }
    }
    None
}
