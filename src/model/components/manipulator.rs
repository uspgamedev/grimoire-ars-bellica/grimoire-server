use super::*;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Manipulator {
    reach: usize,
    strength: usize,
    skill: usize,
    pub carried_entity: Option<ID>,
}

impl Manipulator {
    pub fn new(reach: usize, strength: usize, skill: usize) -> Self {
        Manipulator { reach, strength, skill, carried_entity: None }
    }
    pub fn with_reach(mut self, reach: usize) -> Self {
        self.reach = reach;
        self
    }

    pub fn set_reach(&mut self, reach: usize) {
        self.reach = reach;
    }

    pub fn get_reach(&self) -> usize {
        self.reach
    }

    pub fn get_skill(&self) -> usize {
        self.skill
    }
}

pub fn get_held_entity(world: &World, carrier_entity: ID) -> Option<ID> {
    world.get::<Manipulator>(carrier_entity)?.carried_entity
}

impl Rule<DirectStrikeEvent> for Rulebook<Manipulator> {
    fn apply(&self, world: &mut World, event: &mut DirectStrikeEvent) -> Option<Resolution> {
        let str_power = world.get::<Manipulator>(self.id)?.strength;
        let wpn_power = world.get::<Weapon>(event.weapon_entity)?.power;
        let damage_event = TakeDamageEvent { amount: str_power + wpn_power };
        world.buffer_event(event.target_entity, damage_event);
        Some(Resolution::Partial)
    }
}
