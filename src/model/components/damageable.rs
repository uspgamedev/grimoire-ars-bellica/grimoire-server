use super::*;

use super::seer::find_all_observers;

use core::cmp::min;

#[derive(Debug, Clone, Component, Serialize, Deserialize)]
pub struct Damageable {
    max_hp: usize,
    damage: usize,
}

impl Damageable {
    pub fn new(max_hp: usize) -> Self {
        Self { max_hp, damage: 0 }
    }
    pub fn is_dead(&self) -> bool {
        self.damage >= self.max_hp
    }

    pub fn get_max_hp(&self) -> usize {
        self.max_hp
    }

    pub fn get_hp(&self) -> usize {
        self.max_hp - min(self.max_hp, self.damage)
    }
}

impl Rule<TakeDamageEvent> for Rulebook<Damageable> {
    fn apply(&self, world: &mut World, event: &mut TakeDamageEvent) -> Option<Resolution> {
        let mut damageable = world.get_mut::<Damageable>(self.id)?;
        damageable.damage += min(event.amount, damageable.max_hp);
        Some(Resolution::Partial)
    }
}

impl Rule<TakeDamageEvent> for Rulebook<Visible> {
    fn apply(&self, world: &mut World, event: &mut TakeDamageEvent) -> Option<Resolution> {
        if world.exists::<Damageable>(self.id) {
            for observer_id in find_all_observers(world, self.id) {
                let observe_event = ObserveBehaviorEvent {
                    observed_id: self.id,
                    behavior: format!["took {} damage!", event.amount]
                };
                world.buffer_event(observer_id, observe_event);
            }
        }
        Some(Resolution::Partial)
    }
}

impl Rule<DieEvent> for Rulebook<Damageable> {
    fn apply(&self, world: &mut World, _event: &mut DieEvent) -> Option<Resolution> {
        world.remove::<Damageable>(self.id);
        Some(Resolution::Partial)
    }
}

pub fn check_death(world: &mut World, id: ID) {
    let dead = if let Some(damageable) = world.get::<Damageable>(id) {
        damageable.is_dead()
    } else { false };
    if dead {
        let event = DieEvent {};
        world.buffer_event(id, event);
    }
}
