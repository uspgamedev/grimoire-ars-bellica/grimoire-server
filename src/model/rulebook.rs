use super::component::Component;
use super::components::*;
use super::world::World;

use common::identity::ID;

use std::marker::PhantomData;

pub enum Resolution {
    Final,
    Partial,
}

pub trait Rule<E> {
    fn apply(&self, world: &mut World, event: &mut E) -> Option<Resolution>;
}

impl<T: Component, E> Rule<E> for Rulebook<T> {
    default fn apply(&self, _world: &mut World, _event: &mut E) -> Option<Resolution> {
        None
    }
}

pub struct Rulebook<T: Component> {
    pub id: ID,
    component_type: PhantomData<T>,
}

impl<T: Component> Rulebook<T> {
    fn new(id: ID) -> Self {
        Rulebook {
            id,
            component_type: PhantomData,
        }
    }
}

macro_rules! apply_rule {
    ($($comp_type:ty)+, $world:expr, $id:expr, $event:expr, $rule:expr) => {
        $(
            if $world.get::<$comp_type>($id).is_some() {
                let rulebook = Rulebook::<$comp_type>::new($id);
                $rule = Some(&rulebook);
                if let Some(Resolution::Final) = $rule.unwrap().apply(&mut $world, &mut $event) {
                    return;
                }
            }
        )+
    };
}

pub fn dispatch_event<E>(mut world: &mut World, receiver_id: ID, mut event: E) {
    // NOTE: We need this variable to help infer the exact rule type
    // NOTE: And it needs an underscore to suppress a misplaced warning
    let mut _rule: Option<&dyn Rule<E>> = None;
    apply_to_all_component_types![apply_rule, world, receiver_id, event, _rule];
}
