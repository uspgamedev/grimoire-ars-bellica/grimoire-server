
use common::identity::ID;
use common::directions::Direction;
use common::intn::Int3;
use crate::model::event::EventPack;
use std::collections::HashMap;

#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
pub enum Value {
    Entity(ID),
    EventPack(EventPack),
    Direction(Direction),
    Int3(Int3),
    String(String),
    Integer(i64),
    Float(f64),
    Boolean(bool),
    Array(Vec<Value>)
}

pub type ValueTable = HashMap<String, Value>;

impl Value {
    pub fn as_id(&self) -> Option<ID> {
        if let Value::Entity(id) = self {
            Some(id.clone())
        } else {
            None
        }
    }
    pub fn as_direction(&self) -> Option<Direction> {
        if let Value::Direction(dir) = self {
            Some(dir.clone())
        } else {
            None
        }
    }
    pub fn as_integer(&self) -> Option<i64> {
        if let Value::Integer(i) = self {
            Some(*i)
        } else {
            None
        }
    }
    pub fn as_int3(&self) -> Option<Int3> {
        if let Value::Int3(i3) = self {
            Some(i3.clone())
        } else {
            None
        }
    }
}

impl From<ID> for Value {
    #[inline]
    fn from(val: ID) -> Value {
        Value::Entity(val)
    }
}

impl From<Direction> for Value {
    #[inline]
    fn from(val: Direction) -> Value {
        Value::Direction(val)
    }
}

impl From<bool> for Value {
    #[inline]
    fn from(val: bool) -> Value {
        Value::Boolean(val)
    }
}

impl From<Int3> for Value {
    #[inline]
    fn from(val: Int3) -> Value {
        Value::Int3(val)
    }
}

#[macro_export]
macro_rules! table {
    ( $( $key:expr => $value:expr ),* ) => {
        {
            let mut temp_vec = ValueTable::default();
            $(
                temp_vec.insert(String::from($key), Value::from($value));
            )*
            temp_vec
        }
    };
}

