use common::directions::*;
use common::intn::Int3;

pub fn closest_direction(valid_dirs: &Vec<Direction>, wanted: &Int3) -> Direction {
    let mut closest_dir = Direction::NONE;
    let mut largest_alignment = i64::min_value();
    for dir in valid_dirs.iter() {
        let alignment = Int3::from(dir.clone()).dot(&wanted);
        if alignment > largest_alignment {
            largest_alignment = alignment;
            closest_dir = dir.clone();
        }
    }
    closest_dir
}
