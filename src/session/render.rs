
use super::state::*;
use super::affordances::{get_entity_affordances, get_positional_applications};

use crate::model::components::*;
use crate::model::components::seer::get_all_observed_by;
use crate::model::components::position::get_strict_entity_position;
use crate::model::session::Session;

use common::identity::ID;
use std::collections::HashMap;

pub fn render_state(session: &Session) -> SessionState {
    let mut entities_info = HashMap::default();
    SessionState {
        visible_tilemap: session.world.get_map().clone_tilemap(),
        player_state: render_player_info(&session, &mut entities_info),
        entities_info,
    }
}

fn render_player_info(session: &Session, entities_info: &mut HashMap<ID, EntityInfo>) -> PlayerState {
    let world = &session.world;
    let player_id = world.get_player_id().unwrap();
    for visible_entity_id in get_all_observed_by(world, player_id) {
        let entity_info = render_entity_info(session, visible_entity_id);
        entities_info.insert(visible_entity_id, entity_info);
    }
    let mut inventory = Vec::new();
    for item_id in world.get::<Container>(player_id).unwrap().get_held_entities() {
        let entity_info = render_entity_info(session, item_id);
        inventory.push(entities_info.len());
        entities_info.insert(item_id, entity_info);
    }
    let player_damageable = world.get::<Damageable>(player_id).unwrap();
    PlayerState {
        id: player_id,
        hp: player_damageable.get_hp(),
        max_hp: player_damageable.get_max_hp(),
        reach: world.get::<Manipulator>(player_id).unwrap().get_reach(),
        sight_range: world.get::<Seer>(player_id).unwrap().get_range(),
        move_action: String::from("walk"),
        actions: get_positional_applications(session, player_id),
        inventory,
        journal: world.get::<Player>(player_id).unwrap().journal.clone()
    }
}

fn render_entity_info(session: &Session, id: ID) -> EntityInfo {
    let world = &session.world;
    EntityInfo {
        maybe_position: get_strict_entity_position(world, id),
        passable: !world.exists::<Obstacle>(id),
        appearance: world
            .get::<Visible>(id)
            .unwrap()
            .get_appearance()
            .clone(),
        name: world.get::<Named>(id).unwrap().get_name().clone(),
        id,
        possible_actions: get_entity_affordances(session, world.get_player_id().unwrap(), id),
    }
}

