
use crate::core::ValueTable;

#[derive(Serialize, Deserialize)]
pub enum SessionRequest {
    TryAction(String, ValueTable),
    EndSession,
}
