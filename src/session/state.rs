use common::appearance::Appearance;
use common::intn::Int3;
use common::tilemap::TileMap;
use common::identity::ID;
use crate::core::ValueTable;

use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct SessionState {
    pub visible_tilemap: TileMap,
    pub player_state: PlayerState,
    pub entities_info: HashMap<ID, EntityInfo>,
    //pub interactable_entities: Vec<Interactable>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct EntityInfo {
    pub id: ID,
    pub maybe_position: Option<Int3>,
    pub passable: bool,
    pub appearance: Appearance,
    pub name: String,
    pub possible_actions: Vec<PossibleAction>,
}

#[derive(Serialize, Deserialize)]
pub struct PlayerState {
    pub id: ID,
    pub hp: usize,
    pub max_hp: usize,
    pub reach: usize,
    pub sight_range: usize,
    pub move_action: String,
    pub actions: Vec<PossibleAction>,
    pub inventory: Vec<usize>,
    pub journal: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct PossibleAction {
    pub name: String,
    pub parameters: ValueTable,
}
