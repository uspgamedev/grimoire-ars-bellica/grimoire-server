use super::state::PossibleAction;
use crate::core::{Value, ValueTable};
use crate::model::components::*;
use crate::model::components::applicable::find_all_applicable_by;
use crate::model::components::position::get_entity_position;
use crate::model::components::seer::list_all_visible_positions;
use crate::model::session::Session;
use crate::model::event::Effect;

use common::identity::ID;
use std::io::{Error, ErrorKind, Result};

pub fn try_action(session: &Session, action_name: &String, params: ValueTable) -> Result<Effect> {
    let err = Error::new(ErrorKind::Other, format!["unknown action: {}", action_name]);
    session.action_db.get(action_name).ok_or(err)?.compile(&session.world, params)
}

pub fn get_entity_affordances(
    session: &Session,
    actor_entity: ID,
    target_entity: ID
) -> Vec<PossibleAction> {
    let mut possible_actions = vec![];
    if let Some(interactive) = session.world.get::<Interactive>(target_entity) {
        let params = table![
            "Self" => actor_entity,
            "Target" => target_entity
        ];
        for interaction in &interactive.interactions {
            if let Ok(_) = try_action(session, &interaction, params.clone()) {
                let possible_action = PossibleAction {
                    name: interaction.clone(),
                    parameters: params.clone()
                };
                possible_actions.push(possible_action);
            }
        }
    }
    if let Some(_target_pos) = get_entity_position(&session.world, target_entity) {
        let all_applicable = find_all_applicable_by(&session.world, actor_entity);
        for applicable_entity in all_applicable {
            if let Some(applicable) = session.world.get::<Applicable>(applicable_entity) {
                let params = table![
                    "Self" => actor_entity,
                    "Target" => target_entity,
                    "With" => applicable_entity
                ];
                for application in &applicable.to_targets {
                    match try_action(session, &application, params.clone()) {
                        Ok(_) => {
                            let possible_action = PossibleAction {
                                name: application.clone(),
                                parameters: params.clone()
                            };
                            possible_actions.push(possible_action);
                        },
                        Err(_e) => {
                        }
                    }
                }
            }
        }
    }
    possible_actions
}

pub fn get_positional_applications(session: &Session, actor_entity: ID) -> Vec<PossibleAction> {
    let mut possible_actions = vec![];
    let all_applicable = find_all_applicable_by(&session.world, actor_entity);
    if let Some(actor_pos) = get_entity_position(&session.world, actor_entity) {
        let visible_positions = list_all_visible_positions(&session.world, actor_entity);
        for applicable_entity in all_applicable {
            if let Some(applicable) = session.world.get::<Applicable>(applicable_entity) {
                for application in &applicable.to_positions {
                    for diff in visible_positions.iter() {
                        let params = table![
                            "Self" => actor_entity,
                            "Pos" => &actor_pos + diff,
                            "With" => applicable_entity
                        ];
                        if let Ok(_) = try_action(session, &application, params.clone()) {
                            let possible_action = PossibleAction {
                                name: application.clone(),
                                parameters: params.clone()
                            };
                            possible_actions.push(possible_action);
                        }
                    }
                }
            }
        }
    }
    possible_actions
}
