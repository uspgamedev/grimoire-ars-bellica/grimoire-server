#![feature(min_specialization)]

#[macro_use]
extern crate log;

//#[macro_use] // NOTE: (un)comment if you use datatable![]
extern crate grimoire_common as common;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate lazy_static;

#[macro_use]
pub mod core;
pub mod model;
pub mod session;
#[macro_use]
pub mod script;
pub mod util;
