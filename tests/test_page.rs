extern crate grimoire_common;
extern crate toml;

use grimoire::core::{Value as ObjValue, ValueTable};
use grimoire::model::world::World;
use grimoire::script::definitions::*;
use grimoire::script::interpreter::*;
use grimoire::script::parser::*;
use grimoire::script::tokenizer::*;

fn get_default_interpreter(world: &World) -> PreparationInterpreter {
    PreparationInterpreter { world, registers: ValueTable::default() }
}

#[test]
fn test_parse_walk() {
    let line = String::from("move Self along Dir");
    let tokens = tokenize_string(line).unwrap();
    assert_eq![
        tokens[0],
        [
            Token::MethodIdentifier("move".to_string()),
            Token::ValueIdentifier("Self".to_string()),
            Token::MethodIdentifier("along".to_string()),
            Token::ValueIdentifier("Dir".to_string())
        ]
    ];
    let expression = &parse_executions(tokens).unwrap()[0];
    match expression {
        Expression::Call(call) => {
            assert_eq![call.body.method_ident, "move"];
            assert_eq![call.body.args.len(), 1];
            match &call.body.args[0] {
                Value::Identifier(ident) => assert_eq![ident, "Self"],
                _ => panic![],
            }
            match &call.body.tail {
                Some(tail) => {
                    assert_eq![tail.method_ident, "along"];
                    assert_eq![tail.args.len(), 1];
                    match &tail.args[0] {
                        Value::Identifier(ident) => assert_eq![ident, "Dir"],
                        _ => panic![],
                    }
                }
                None => panic![],
            }
        }
        _ => panic![],
    }
}

#[test]
fn test_if_statement() {
    let line = String::from("if true then 2");
    let tokens = tokenize_string(line).unwrap();
    assert_eq![
        tokens[0],
        [Token::If, Token::True, Token::Then, Token::NumberLiteral(2)]
    ];
    let world = World::default();
    let expression = &parse_executions(tokens).unwrap()[0];
    let mut interpreter = get_default_interpreter(&world);
    let value = interpreter.interpret_expr(expression).unwrap();
    assert_eq![value, ObjValue::Integer(2)];
}

#[test]
fn test_if_else_statement() {
    let line = String::from("if false then 2 else 4");
    let tokens = tokenize_string(line).unwrap();
    assert_eq![
        tokens[0],
        [
            Token::If,
            Token::False,
            Token::Then,
            Token::NumberLiteral(2),
            Token::Else,
            Token::NumberLiteral(4)
        ]
    ];
    let world = World::default();
    let expression = &parse_executions(tokens).unwrap()[0];
    let mut interpreter = get_default_interpreter(&world);
    let value = interpreter.interpret_expr(expression).unwrap();
    assert_eq![value, ObjValue::Integer(4)];
}
